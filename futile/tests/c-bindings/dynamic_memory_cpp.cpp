#include "Misc"
#include "DynamicMemory"
#include "YamlOutput"
#include "YamlStrings"
#include "config.h"

extern "C" {
    void FC_FUNC(test_alloc_int_1d, TEST_ALLOC_INT_1D)(Futile::Int1dPointer::f90_int_1d_pointer*, size_t*);
    void FC_FUNC(test_alloc_dbl_2d, TEST_ALLOC_DBL_2D)(Futile::Double2dPointer::f90_double_2d_pointer*, size_t*);
}

void test_alloc_int_1d(Futile::Int1dPointer &array)
{
    Futile::Int1dPointer::f90_int_1d_pointer ptr;
    size_t len;

    FC_FUNC(test_alloc_int_1d, TEST_ALLOC_INT_1D)(&ptr, &len);
    array = Futile::Int1dPointer(ptr, len);
}

void test_alloc_dbl_2d(Futile::Double2dPointer &array)
{
    Futile::Double2dPointer::f90_double_2d_pointer ptr;
    size_t len[2];

    FC_FUNC(test_alloc_dbl_2d, TEST_ALLOC_DBL_2D)(&ptr, len);
    array = Futile::Double2dPointer(ptr, len);
}

int main(int argc, char **argv)
{
    Futile::initialize();

    {
        Futile::Int1dPointer array;
        test_alloc_int_1d(array);

        Futile::Yaml::mapping_open("integer 1D array");
        Futile::Yaml::map("size", array.size());
        array[2] = 33;
        bool flow = true;
        Futile::Yaml::sequence_open("values", NULL, NULL, &flow);
        for (auto &v: array) {
            char str[95];
            Futile::yaml_toa(str, v);
            Futile::Yaml::sequence(str);
        }
        Futile::Yaml::sequence_close();
        Futile::Yaml::mapping_close();
    }
    
    {
        Futile::Double2dPointer array;
        test_alloc_dbl_2d(array);

        Futile::Yaml::mapping_open("double 2D array");
        Futile::Yaml::map("size", array.size());
        array[{1, 2}] = 33.;
        Futile::Yaml::sequence_open("values");
        for (auto &v: array.sliced()) {
            char str[95];
            Futile::yaml_toa(str, &v, array.shape(0));
            Futile::Yaml::sequence(str);
        }
        Futile::Yaml::sequence_close();
        Futile::Yaml::mapping_close();
    }
    
    Futile::finalize();

    return 0;
}
