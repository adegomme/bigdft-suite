#ifndef YAML_OUTPUT_H
#define YAML_OUTPUT_H

#include "futile_cst.h"
#include "FUtils"
#include "YamlStrings"
#include "Dict"
#include "FEnums"

namespace Futile {

namespace Yaml {

void swap_stream(int new_unit,
    int& old_unit,
    int& ierr);

void output_errors(void);

void set_default_stream(int unit,
    int& ierr);

void get_default_stream(int& unit);

void stream_connected(const char* filename,
    int& unit,
    int (*istat) = nullptr);

void set_stream(const int (*unit) = nullptr,
    const char (*filename) = nullptr,
    int (*istat) = nullptr,
    const int (*tabbing) = nullptr,
    const int (*record_length) = nullptr,
    const char (*position) = nullptr,
    const bool (*setdefault) = nullptr);

void stream_attributes(const int (*unit) = nullptr,
    const int (*stream_unit) = nullptr,
    int (*icursor) = nullptr,
    bool (*flowrite) = nullptr,
    int (*itab_active) = nullptr,
    int (*iflowlevel) = nullptr,
    int (*ilevel) = nullptr,
    int (*ilast) = nullptr,
    int (*indent) = nullptr,
    int (*indent_previous) = nullptr,
    int (*record_length) = nullptr);

void new_document(const int (*unit) = nullptr);

void flush_document(const int (*unit) = nullptr);

void close_stream(const int (*unit) = nullptr,
    int (*istat) = nullptr);

void close_all_streams(void);

void dict_inspect(Dictionary& dict);

void dump_progress_bar(FProgressBar& bar,
    const int (*step) = nullptr,
    const int (*unit) = nullptr);

void cite(const char* paper,
    const int (*unit) = nullptr);

void bib_dump(Dictionary& citations,
    const int (*unit) = nullptr);

void scalar(const char* message,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*hfill) = nullptr);

void mapping_open(const char (*mapname) = nullptr,
    const char (*label) = nullptr,
    const char (*tag) = nullptr,
    const bool (*flow) = nullptr,
    const int (*tabbing) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr);

void mapping_close(const char (*advance) = nullptr,
    const int (*unit) = nullptr);

void sequence_open(const char (*mapname) = nullptr,
    const char (*label) = nullptr,
    const char (*tag) = nullptr,
    const bool (*flow) = nullptr,
    const int (*tabbing) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr);

void sequence_close(const char (*advance) = nullptr,
    const int (*unit) = nullptr);

void newline(const int (*unit) = nullptr);

void sequence(const char (*seqvalue) = nullptr,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const int (*padding) = nullptr);

void dict_dump(const Dictionary& dict,
    const int (*unit) = nullptr,
    const bool (*flow) = nullptr,
    const bool (*verbatim) = nullptr);

void dict_dump_all(const Dictionary& dict,
    const int (*unit) = nullptr,
    const bool (*flow) = nullptr,
    const bool (*verbatim) = nullptr);

void release_document(const int (*unit) = nullptr);

void map(const char* mapname,
    const char* mapvalue,
    const char (*label) = nullptr,
    const char (*tag) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr);

void map(const char* mapname,
    const Dictionary& mapvalue,
    const char (*label) = nullptr,
    const int (*unit) = nullptr,
    const bool (*flow) = nullptr);

void map(const char* mapname,
    const FEnumerator& mapvalue,
    const char (*label) = nullptr,
    const int (*unit) = nullptr,
    const bool (*flow) = nullptr);

void map(const char* mapname,
    size_t mapvalue,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    int mapvalue,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    float mapvalue,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    double mapvalue,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    bool mapvalue,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    const double* mapvalue,
    size_t mapvalue_dim_0,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    const float* mapvalue,
    size_t mapvalue_dim_0,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

/* void map(const char* mapname); */

/* void map(const char* mapname); */

void map(const char* mapname,
    const size_t* mapvalue,
    size_t mapvalue_dim_0,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    const int* mapvalue,
    size_t mapvalue_dim_0,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    const double* mapvalue,
    size_t mapvalue_dim_0,
    size_t mapvalue_dim_1,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    const float* mapvalue,
    size_t mapvalue_dim_0,
    size_t mapvalue_dim_1,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void map(const char* mapname,
    const int* mapvalue,
    size_t mapvalue_dim_0,
    size_t mapvalue_dim_1,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

/* void map(const char* mapname); */

void map(const char* mapname,
    const double* mapvalue,
    size_t mapvalue_dim_0,
    size_t mapvalue_dim_1,
    size_t mapvalue_dim_2,
    const char (*label) = nullptr,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*fmt) = nullptr);

void warning(const FString& message,
    const int (*level) = nullptr,
    const int (*unit) = nullptr);

void warning(const char* message,
    const int (*level) = nullptr,
    const int (*unit) = nullptr);

void comment(const FString& message,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*hfill) = nullptr,
    const int (*tabbing) = nullptr);

void comment(const char* message,
    const char (*advance) = nullptr,
    const int (*unit) = nullptr,
    const char (*hfill) = nullptr,
    const int (*tabbing) = nullptr);


}

}
#endif
