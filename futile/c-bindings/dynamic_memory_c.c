#include "dynamic_memory.h"
#include "config.h"

void FC_FUNC(f_free_ptr_i1, F_FREE_PTR_I1)(f90_int_1d_pointer*);
void FC_FUNC(f_free_ptr_d2, F_FREE_PTR_D2)(f90_double_2d_pointer*);

void int_1d_pointer_free(int_1d_pointer *ptr)
{
    FC_FUNC(f_free_ptr_i1, F_FREE_PTR_I1)(&ptr->ptr);
}

void double_2d_pointer_free(double_2d_pointer *ptr)
{
    FC_FUNC(f_free_ptr_d2, F_FREE_PTR_D2)(&ptr->ptr);
}
