#ifndef YAML_OUTPUT_H
#define YAML_OUTPUT_H

#include "futile_cst.h"
#include <stdbool.h>
#include "f_utils.h"
#include "yaml_strings.h"
#include "dict.h"
#include "f_enums.h"


void yaml_swap_stream(int new_unit,
  int* old_unit,
  int* ierr);

void yaml_output_errors(void);

void yaml_set_default_stream(int unit,
  int* ierr);

void yaml_get_default_stream(int* unit);

void yaml_stream_connected(const char* filename,
  int* unit,
  int (*istat));

void yaml_set_stream(const int (*unit),
  const char (*filename),
  int (*istat),
  const int (*tabbing),
  const int (*record_length),
  const char (*position),
  const bool (*setdefault));

void yaml_stream_attributes(const int (*unit),
  const int (*stream_unit),
  int (*icursor),
  bool (*flowrite),
  int (*itab_active),
  int (*iflowlevel),
  int (*ilevel),
  int (*ilast),
  int (*indent),
  int (*indent_previous),
  int (*record_length));

void yaml_new_document(const int (*unit));

void yaml_flush_document(const int (*unit));

void yaml_close_stream(const int (*unit),
  int (*istat));

void yaml_close_all_streams(void);

void yaml_dict_inspect(f90_dictionary_pointer dict);

void dump_progress_bar(f90_f_progress_bar* bar,
  const int (*step),
  const int (*unit));

void yaml_cite(const char* paper,
  const int (*unit));

void yaml_bib_dump(f90_dictionary_pointer citations,
  const int (*unit));

void yaml_scalar(const char* message,
  const char (*advance),
  const int (*unit),
  const char (*hfill));

void yaml_mapping_open(const char (*mapname),
  const char (*label),
  const char (*tag),
  const bool (*flow),
  const int (*tabbing),
  const char (*advance),
  const int (*unit));

void yaml_mapping_close(const char (*advance),
  const int (*unit));

void yaml_sequence_open(const char (*mapname),
  const char (*label),
  const char (*tag),
  const bool (*flow),
  const int (*tabbing),
  const char (*advance),
  const int (*unit));

void yaml_sequence_close(const char (*advance),
  const int (*unit));

void yaml_newline(const int (*unit));

void yaml_sequence(const char (*seqvalue),
  const char (*label),
  const char (*advance),
  const int (*unit),
  const int (*padding));

void yaml_dict_dump(f90_dictionary_pointer dict,
  const int (*unit),
  const bool (*flow),
  const bool (*verbatim));

void yaml_dict_dump_all(f90_dictionary_pointer dict,
  const int (*unit),
  const bool (*flow),
  const bool (*verbatim));

void yaml_release_document(const int (*unit));

void yaml_map(const char* mapname,
  const char* mapvalue,
  const char (*label),
  const char (*tag),
  const char (*advance),
  const int (*unit));

void yaml_map_dict(const char* mapname,
  f90_dictionary_pointer mapvalue,
  const char (*label),
  const int (*unit),
  const bool (*flow));

void yaml_map_enum(const char* mapname,
  const f90_f_enumerator* mapvalue,
  const char (*label),
  const int (*unit),
  const bool (*flow));

void yaml_map_li(const char* mapname,
  size_t mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_i(const char* mapname,
  int mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_f(const char* mapname,
  float mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_d(const char* mapname,
  double mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_l(const char* mapname,
  bool mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_dv(const char* mapname,
  const double* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_rv(const char* mapname,
  const float* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

/* void yaml_map_cv(const char* mapname); */

/* void yaml_map_lv(const char* mapname); */

void yaml_map_liv(const char* mapname,
  const size_t* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_iv(const char* mapname,
  const int* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_dm(const char* mapname,
  const double* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_rm(const char* mapname,
  const float* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_map_im(const char* mapname,
  const int* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

/* void yaml_map_lm(const char* mapname); */

void yaml_map_dt(const char* mapname,
  const double* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  size_t mapvalue_dim_2,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt));

void yaml_warning_str(const f90_f_string* message,
  const int (*level),
  const int (*unit));

void yaml_warning_c(const char* message,
  const int (*level),
  const int (*unit));

void yaml_comment_str(const f90_f_string* message,
  const char (*advance),
  const int (*unit),
  const char (*hfill),
  const int (*tabbing));

void yaml_comment_c(const char* message,
  const char (*advance),
  const int (*unit),
  const char (*hfill),
  const int (*tabbing));

#endif
