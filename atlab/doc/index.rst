.. _atlab_index:

Welcome to atlab's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Modules:

   domain
   
   box
   
   harmonics

   numerics

   mp_quadrature

   iobox

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Continuous integration
----------------------

.. image:: https://gitlab.com/l_sim/atlab/badges/devel/pipeline.svg
   :target: https://gitlab.com/l_sim/atlab/-/commits/devel

.. image:: https://gitlab.com/l_sim/atlab/badges/devel/coverage.svg
   :target: https://l_sim.gitlab.io/atlab/report
