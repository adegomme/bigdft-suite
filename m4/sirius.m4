# -*- Autoconf -*-
#
# Copyright (c) 2020 BigDFT Group (D. Caliste, L.Genovese)
# All rights reserved.
#
# This file is part of the BigDFT software package. For license information,
# please see the COPYING file in the top-level directory of the BigDFT source
# distribution.
#
dnl we might think of inserting this option in the suitepkg.m4 file
AC_DEFUN([AX_SIRIUS],
[dnl Test for sirius
ax_have_SIRIUS_search="yes"
AC_ARG_ENABLE(sirius, AS_HELP_STRING([--enable-sirius], [Enable detection of sirius compilation (enabled by default).]),
			 ax_have_SIRIUS_search=$enableval, ax_have_SIRIUS_search="yes")
if test x"$ax_have_SIRIUS_search" = "xyes" ; then

ax_tmp_incdirs=""
if test -n "$C_INCLUDE_PATH" ; then
  for path in ${C_INCLUDE_PATH//:/ }; do
    ax_tmp_incdirs="$ax_tmp_incdirs -I$path/sirius"
  done
fi

AX_PACKAGE([SIRIUS],[6.5],[-lsirius],[],[$ax_tmp_incdirs],
           [
program test
use iso_c_binding
use sirius
logical(c_bool) :: withMPI = .false.
call sirius_initialize(withMPI)
end program
],
           [
use iso_c_binding
use sirius
logical(c_bool) :: withMPI = .false.
call sirius_initialize(withMPI)
],
           [], [],[])
else
  ax_have_SIRIUS="no"
fi
])
