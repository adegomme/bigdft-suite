<?xml version="1.0"?><!--*- mode: nxml; indent-tabs-mode: nil -*-->
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<!-- vim:set ts=2 expandtab: -->
<moduleset>

  <!-- Official repositories, not used by default, using provided tars instead. -->

  <repository type="tarball" name="mstore" href="https://github.com/grimme-lab/mstore/releases/download/v0.2.0/"/>
  <repository type="tarball" name="test-drive" href="https://github.com/fortran-lang/test-drive/releases/download/v0.4.0/"/>
  <repository type="tarball" name="toml-f" href="https://github.com/toml-f/toml-f/releases/download/v0.3.1/"/>
  <repository type="tarball" name="json-fortran" href="https://github.com/jacobwilliams/json-fortran/archive/refs/tags/"/>
  <repository type="tarball" name="mctc-lib" href="https://github.com/grimme-lab/mctc-lib/archive/refs/tags/"/>
  <repository type="tarball" name="dftd3" href="https://github.com/dftd3/simple-dftd3/archive/refs/tags/"/>
  <repository type="tarball" name="s-dftd3" href="./dftd3/python/"/>
  <repository type="git" name="dftd4" href="https://github.com/dftd4/"/>
  <repository type="tarball" name="pydftd4" href="./dftd4/python/"/>

  <include href="lowlevel.modules"/>

  <cmake id="json-fortran" use-ninja="no">
    <branch repo="json-fortran" module="8.2.5.tar.gz"
            version="8.2.5" checkoutdir="json-fortran"/>
    <cmakeargs value="-DUSE_GNU_INSTALL_CONVENTION=Yes -DENABLE_IN_SOURCE_BUILDS=ON"/>
  </cmake>

  <cmake id="mctc-lib">
    <branch repo="mctc-lib"  module="v0.3.1.tar.gz" version="0.3.1" checkoutdir="mctc-lib"/>
    <dependencies>
      <dep package="json-fortran"/>
      <dep package="toml-f"/>
    </dependencies>
  </cmake>

  <meson id="mstore">
    <branch repo="mstore"  module="mstore-0.2.0.tar.xz" version="0.2.0" checkoutdir="mstore"/>
  </meson>

  <meson id="test-drive">
    <branch repo="test-drive"  module="test-drive-0.4.0.tar.xz" version="0.4.0" checkoutdir="test-drive"/>
  </meson>

  <meson id="toml-f">
    <branch repo="toml-f"  module="toml-f-0.3.1.tar.xz" version="0.3.1" checkoutdir="toml-f"/>
    <dependencies>
      <dep package="test-drive"/>
    </dependencies>
  </meson>

  <meson id="dftd3">
    <branch repo="dftd3"  module="v0.6.0.tar.gz" version="0.6.0" checkoutdir="dftd3"/>
    <mesonargs value="-Dpython=true -Dpython_version=$(which python3)"/>
    <dependencies>
      <dep package="mctc-lib"/>
      <dep package="mstore"/>
    </dependencies>
  </meson>

  <distutils id="s-dftd3" supports-non-srcdir-builds='no'>
    <branch repo="s-dftd3" module="s-dft3-0.6.0.tar.gz" version="0.6.0" checkoutdir="dftd3/python">
    </branch>
    <dependencies>
      <dep package="dftd3"/>
    </dependencies>
  </distutils>

  <meson id="dftd4">
    <branch repo="dftd4"  module="dftd4"
            checkoutdir="dftd4"/>
    <mesonargs value="-Dlapack=mkl-rt -Dpython_version=$(which python3)"/>
  </meson>

  <distutils id="pydftd4">
    <branch repo="pydftd4" module="dftd4-3.4.0.tar.gz" version="3.4.0" checkoutdir="dftd4/python">
    </branch>
    <dependencies>
      <dep package="dftd4"/>
    </dependencies>
  </distutils>

</moduleset>
