//! @file
//!  Wrappers for OpenCL
//!
//! @author
//!    Copyright (C) 2009-2011 BigDFT group 
//!    This file is distributed under the terms of the
//!    GNU General Public License, see ~/COPYING file
//!    or http://www.gnu.org/copyleft/gpl.txt .
//!    For the list of contributors, see ~/AUTHORS 


#include "liborbs_ocl.h"
#include "Uncompress.h"
#include "Wavelet.h"
#include "Reduction.h"
#include "MagicFilter.h"

#include "config.h"

void ocl_locden(liborbs_command_queue *command_queue,
                const cl_uint dimensions[3],
                cl_double hfac,
                cl_uint iaddjmp,
                cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                cl_mem psi_c, cl_mem psi_f,
                cl_mem psi, cl_mem out, cl_mem work,
                cl_mem pot)
{
   uncompress_d(command_queue, dimensions,
                nseg_c, nvctr_c, keyg_c, keyv_c,
                nseg_f, nvctr_f, keyg_f, keyv_f,
                psi_c, psi_f, out);
   syn_self_d(command_queue, dimensions, out, psi);
   magicfilter_den_d(command_queue, dimensions, work, psi, out);
   cl_uint offset=0;
   cl_uint ndat = 8 * dimensions[0] * dimensions[1] * dimensions[2];
   axpy_offset_self_d(command_queue, ndat, hfac, offset, out, iaddjmp, pot);
}
void FC_FUNC_(f_locden,F_LOCDEN)(liborbs_command_queue **command_queue,
                                     cl_uint *dimensions,
                                     cl_double *hfac,
                                     cl_uint *iaddjmp,
                                     cl_uint *nseg_c, cl_uint *nvctr_c, cl_mem *keyg_c, cl_mem *keyv_c, 
                                     cl_uint *nseg_f, cl_uint *nvctr_f, cl_mem *keyg_f, cl_mem *keyv_f,
                                     cl_mem *psi_c, cl_mem *psi_f,
                                     cl_mem *psi, cl_mem *out, cl_mem *work,
                                     cl_mem *pot)
{
    ocl_locden(*command_queue, dimensions, *hfac, *iaddjmp,
               *nseg_c, *nvctr_c, *keyg_c, *keyv_c,
               *nseg_f, *nvctr_f, *keyg_f, *keyv_f,
               *psi_c, *psi_f, *psi, *out, *work, *pot);
}

void ocl_locden_generic(liborbs_command_queue *command_queue,
                        const cl_uint dimensions[3], const cl_uint periodic[3],
                        cl_double hfac,
                        cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                        cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                        cl_mem psi_c, cl_mem psi_f,
                        cl_mem psi, cl_mem out, cl_mem work,
                        cl_mem pot)
{
   uncompress_d(command_queue, dimensions,
                nseg_c, nvctr_c, keyg_c, keyv_c,
                nseg_f, nvctr_f, keyg_f, keyv_f,
                psi_c, psi_f, out);
   syn_self_d_generic(command_queue, dimensions, periodic, out, psi);
   magicfilter_den_d_generic(command_queue, dimensions, periodic,
                             work, psi, out);
   cl_uint ndat = (dimensions[0]*2 + (periodic[0]?0:14+15)) * (dimensions[1]*2 + (periodic[1]?0:14+15)) * (dimensions[2]*2 + (periodic[2]?0:14+15));
   axpy_self_d(command_queue, ndat, hfac, out, pot);
}
void FC_FUNC_(f_locden_generic,F_LOCDEN_GENERIC)(liborbs_command_queue **command_queue,
                                     cl_uint *dimensions,
                                     cl_uint *periodic,
                                     cl_double *hfac,
                                     cl_uint *nseg_c, cl_uint *nvctr_c, cl_mem *keyg_c, cl_mem *keyv_c, 
                                     cl_uint *nseg_f, cl_uint *nvctr_f, cl_mem *keyg_f, cl_mem *keyv_f,
                                     cl_mem *psi_c, cl_mem *psi_f,
                                     cl_mem *psi, cl_mem *out, cl_mem *work,
                                     cl_mem *pot)
{
    ocl_locden_generic(*command_queue, dimensions, periodic, *hfac,
                       *nseg_c, *nvctr_c, *keyg_c, *keyv_c,
                       *nseg_f, *nvctr_f, *keyg_f, *keyv_f,
                       *psi_c, *psi_f, *psi, *out, *work, *pot);
}

