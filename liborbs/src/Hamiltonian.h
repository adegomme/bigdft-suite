#ifndef HAMILTONIAN_H
#define HAMILTONIAN_H

#include "liborbs_ocl.h"

/** Performs a full local hamiltonian in periodic boundary conditions.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param k k-points values in the reduced Brillouin zone.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the compressed data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the uncompressed data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the compressed data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the uncompressed data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input data.
 *  @param pot array of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double) containing potential input data.
 *  @param work1 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param work2 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param work3 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param work4 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param epot potential energy of the system.
 *  @param ekinpot kinetic energy of the system.
*/
void ocl_fulllocham(liborbs_command_queue *command_queue,
                    const cl_uint dimensions[3], const cl_double h[3],
                    cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                    cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                    cl_mem psi_c, cl_mem psi_f,
                    cl_mem pot, 
                    cl_mem psi, cl_mem out,
                    cl_mem work, cl_mem kinres,
                    cl_double *epot,cl_double *ekinpot);

/** Performs a full local hamiltonian on a compressed wave function.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the compressed data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the uncompressed data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the compressed data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the uncompressed data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input and output data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input and output data.
 *  @param pot array of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double) containing potential input data.
 *  @param work1 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work2 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work3 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work4 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param epot potential energy of the system.
 *  @param ekinpot kinetic energy of the system.
*/
void ocl_fulllocham_generic(liborbs_command_queue *command_queue,
                            const cl_uint dimensions[3], const cl_uint periodic[3],
                            const cl_double h[3],
                            cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                            cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                            cl_mem psi_c, cl_mem psi_f,
                            cl_mem pot, 
                            cl_mem psi, cl_mem out,
                            cl_mem work, cl_mem kinres,
                            cl_double *epot,cl_double *ekinpot);

/** Performs a full local hamiltonian on a compressed wave function.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the compressed data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the uncompressed data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the compressed data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the uncompressed data.
 *  @param psi_c_r array of size nvctr_c * sizeof(double), containing coarse input and output real data.
 *  @param psi_f_r array of size nvctr_f * 7 * sizeof(double), containing fine input and output real data.
 *  @param psi_c_i array of size nvctr_c * sizeof(double), containing coarse input and output imaginary data.
 *  @param psi_f_i array of size nvctr_f * 7 * sizeof(double), containing fine input and output imaginary data.
 *  @param pot array of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double) containing potential input data.
 *  @param psi_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param out_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param psi_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param out_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param kinres_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param kinres_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param nspinor identifies the number of internal components used in the computation (1: real only, 2: both, 4: unsupported).
 *  @param epot potential energy of the system.
 *  @param ekinpot kinetic energy of the system.
*/
void ocl_fulllocham_generic_k(liborbs_command_queue *command_queue,
                              const cl_uint dimensions[3], const cl_uint periodic[3],
                              const cl_double h[3], const cl_double k[3],
                              cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                              cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                              cl_mem psi_c_r, cl_mem psi_f_r,
                              cl_mem psi_c_i, cl_mem psi_f_i,
                              cl_mem pot, 
                              cl_mem psi_r, cl_mem out_r, cl_mem work_r,
                              cl_mem psi_i, cl_mem out_i, cl_mem work_i,
                              cl_mem kinres_r,
                              cl_mem kinres_i,
                              cl_uint nspinor,
                              cl_double *epot,cl_double *ekinpot);

void ocl_isf_to_daub(liborbs_command_queue *command_queue,
                     const cl_uint dimensions[3], const cl_uint periodic[3],
                     cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                     cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                     cl_mem psi_c, cl_mem psi_f,
                     cl_mem psi, cl_mem out,
                     cl_mem work, cl_mem kinres);

void ocl_daub_to_isf(liborbs_command_queue *command_queue,
                     const cl_uint dimensions[3], const cl_uint periodic[3],
                     cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                     cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                     cl_mem psi_c, cl_mem psi_f,
                     cl_mem psi, cl_mem out,
                     cl_mem work, cl_mem kinres);

#endif
