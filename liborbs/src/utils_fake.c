#include "config.h"

void FC_FUNC_(ocl_release_mem_object,OCL_RELEASE_MEM_OBJECT)() {
}

void FC_FUNC_(ocl_create_read_buffer,OCL_CREATE_READ_BUFFER)() {
}

void FC_FUNC_(ocl_enqueue_write_buffer,OCL_ENQUEUE_WRITE_BUFFER)() {
}

void FC_FUNC_(ocl_pin_read_buffer_async,OCL_PIN_READ_BUFFER_ASYNC)() {
}

void FC_FUNC_(ocl_pin_read_buffer,OCL_PIN_READ_BUFFER)() {
}

void FC_FUNC_(ocl_create_read_write_buffer,OCL_CREATE_READ_WRITE_BUFFER)() {
}

void FC_FUNC_(ocl_enqueue_write_buffer_async,OCL_ENQUEUE_WRITE_BUFFER_ASYNC)() {
}

void FC_FUNC_(ocl_enqueue_read_buffer,OCL_ENQUEUE_READ_BUFFER)() {
}

void FC_FUNC_(ocl_enqueue_read_buffer_async,OCL_ENQUEUE_READ_BUFFER_ASYNC)() {
}

void FC_FUNC_(ocl_unmap_mem_object,OCL_UNMAP_MEM_OBJECT)() {
}

void FC_FUNC_(ocl_map_read_buffer_async,OCL_MAP_READ_BUFFER_ASYNC)() {
}

void FC_FUNC_(f_daub_to_isf,F_DAUB_TO_ISF)() {
}

void FC_FUNC_(f_isf_to_daub,F_ISF_TO_DAUB)() {
}

void FC_FUNC_(f_fulllocham_generic_k,F_FULLLOCHAM_GENERIC_K)() {
}

void FC_FUNC_(f_preconditioner_generic_k,F_PRECONDITIONER_GENERIC_K)() {
}

void FC_FUNC_(f_locden_generic,F_LOCDEN_GENERIC)() {
}

void FC_FUNC_(ocl_map_write_buffer_async,OCL_MAP_WRITE_BUFFER_ASYNC)() {
}
