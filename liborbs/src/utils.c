#define _GNU_SOURCE
#include <string.h>

#include "liborbs_utils.h"
#include "liborbs_ocl.h"
#include "config.h"

#include "Uncompress.h"
#include "Wavelet.h"
#include "Reduction.h"
#include "MagicFilter.h"
#include "Kinetic.h"

/** Activate debugging info. */
#define DEBUG 0
/** Activate profiling info. */
#define PROFILING 0

cl_device_id oclGetFirstDev(cl_context cxGPUContext)
{
    size_t szParmDataBytes;
    cl_device_id* cdDevices;

    // get the list of GPU devices associated with context
    clGetContextInfo(cxGPUContext, CL_CONTEXT_DEVICES, 0, NULL, &szParmDataBytes);
    cdDevices = (cl_device_id*) malloc(szParmDataBytes);

    clGetContextInfo(cxGPUContext, CL_CONTEXT_DEVICES, szParmDataBytes, cdDevices, NULL);

    cl_device_id first = cdDevices[0];
    free(cdDevices);

    return first;
}

size_t shrRoundUp(size_t group_size, size_t global_size)
{
    size_t r = global_size % group_size;
    if(r == 0)
    {
        return global_size;
    } else
    {
        return global_size + group_size - r;
    }
}

static const char * initialize_program="\
#ifdef cl_khr_fp64\n\
#pragma OPENCL EXTENSION cl_khr_fp64: enable \n\
#elif defined (cl_amd_fp64)\n\
#pragma OPENCL EXTENSION cl_amd_fp64: enable \n\
#endif\n\
__kernel void c_initializeKernel_d(uint n, __global const double * x_in, __global double * y_in, double c) {\n\
size_t ig = get_global_id(0);\n\
ig = get_group_id(0) == get_num_groups(0) - 1 ? ig - ( get_global_size(0) - n ) : ig;\n\
y_in[ig] = x_in[ig] * c;\n\
};\n\
__kernel void v_initializeKernel_d(uint n, __global double * y_in, double v) {\n\
size_t ig = get_global_id(0);\n\
ig = get_group_id(0) == get_num_groups(0) - 1 ? ig - ( get_global_size(0) - n ) : ig;\n\
y_in[ig] = v;\n\
};\n\
__kernel void p_initializeKernel_d(uint n, __global const double * x, __global double * y) {\n\
size_t ig = get_global_id(0);\n\
ig = get_group_id(0) == get_num_groups(0) - 1 ? ig - ( get_global_size(0) - n ) : ig;\n\
y[ig] = x[ig] * x[ig];\n\
};\n\
";

static void build_initialize_programs(liborbs_context * context){
    cl_int ciErrNum = CL_SUCCESS;

    context->initializeProgram = clCreateProgramWithSource(context->context,1,(const char**) &initialize_program, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to create program!");
    ciErrNum = clBuildProgram(context->initializeProgram, 0, NULL, "-cl-mad-enable", NULL, NULL);
    if (ciErrNum != CL_SUCCESS)
    {
        fprintf(stderr,"Error: Failed to build c_initialize program!\n");
        char cBuildLog[10240];
        clGetProgramBuildInfo(context->initializeProgram, oclGetFirstDev(context->context), CL_PROGRAM_BUILD_LOG,sizeof(cBuildLog), cBuildLog, NULL );
	fprintf(stderr,"%s\n",cBuildLog);
        exit(1);
    }
}

static void create_initialize_kernels(liborbs_context * context, struct liborbs_kernels * kernels){
    cl_int ciErrNum = CL_SUCCESS;
    kernels->c_initialize_kernel_d=clCreateKernel(context->initializeProgram,"c_initializeKernel_d",&ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to create c_initializeKernel_d kernel!");
    kernels->v_initialize_kernel_d=clCreateKernel(context->initializeProgram,"v_initializeKernel_d",&ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to create v_initializeKernel_d kernel!");
}

void v_initialize_generic(cl_kernel kernel, cl_command_queue command_queue, cl_uint *ndat, cl_mem *out, double *c) {
  cl_int ciErrNum;
  size_t block_size_i=64;
  assert(*ndat>=block_size_i);
  cl_uint i=0;
  clSetKernelArg(kernel, i++,sizeof(*ndat), (void*)ndat);
  clSetKernelArg(kernel, i++,sizeof(*out), (void*)out);
  clSetKernelArg(kernel, i++,sizeof(*c), (void*)c);
  size_t localWorkSize[] = { block_size_i };
  size_t globalWorkSize[] ={ shrRoundUp(block_size_i,*ndat) };
  ciErrNum = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
  oclErrorCheck(ciErrNum,"Failed to enqueue v_initialize kernel!");
}

void c_initialize_generic(cl_kernel kernel, cl_command_queue command_queue, cl_uint *ndat, cl_mem *in, cl_mem *inout, double *c) {
  cl_int ciErrNum;
  size_t block_size_i=64;
  assert(*ndat>=block_size_i);
  cl_uint i=0;
  clSetKernelArg(kernel, i++,sizeof(*ndat), (void*)ndat);
  clSetKernelArg(kernel, i++,sizeof(*in), (void*)in);
  clSetKernelArg(kernel, i++,sizeof(*inout), (void*)inout);
  clSetKernelArg(kernel, i++,sizeof(*c), (void*)c);
  size_t localWorkSize[] = { block_size_i };
  size_t globalWorkSize[] ={ shrRoundUp(block_size_i,*ndat) };
  ciErrNum = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
  oclErrorCheck(ciErrNum,"Failed to enqueue c_initialize kernel!");
}

static void clean_initialize_kernels(struct liborbs_kernels * kernels){
  cl_int ciErrNum;
  ciErrNum = clReleaseKernel(kernels->c_initialize_kernel_d);
  oclErrorCheck(ciErrNum,"Failed to release kernel!");
  ciErrNum = clReleaseKernel(kernels->v_initialize_kernel_d);
  oclErrorCheck(ciErrNum,"Failed to release kernel!");
}

static void clean_initialize_programs(liborbs_context * context){
  cl_int ciErrNum;
  ciErrNum = clReleaseProgram(context->initializeProgram);
  oclErrorCheck(ciErrNum,"Failed to release program!");
}

struct _opencl_version opencl_version_1_0 = {1,0};
struct _opencl_version opencl_version_1_1 = {1,1};
struct _opencl_version opencl_version_1_2 = {1,2};

cl_int compare_opencl_version(struct _opencl_version v1, struct _opencl_version v2) {
  if(v1.major > v2.major)
    return 1;
  if(v1.major < v2.major)
    return -1;
  if(v1.minor > v2.minor)
    return 1;
  if(v1.minor < v2.minor)
    return -1;
  return 0;
}

static void get_device_infos(cl_device_id device, struct liborbs_device_infos * infos){
    clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(infos->DEVICE_TYPE), &(infos->DEVICE_TYPE), NULL);
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(infos->LOCAL_MEM_SIZE), &(infos->LOCAL_MEM_SIZE), NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(infos->MAX_WORK_GROUP_SIZE), &(infos->MAX_WORK_GROUP_SIZE), NULL);
    clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(infos->MAX_COMPUTE_UNITS), &(infos->MAX_COMPUTE_UNITS), NULL);
    clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(infos->NAME), infos->NAME, NULL);
}

// WARNING : devices are supposed to be uniform in a context
void get_context_devices_infos(liborbs_context * context, struct liborbs_device_infos * infos){
    cl_uint device_number;

#ifdef CL_VERSION_1_1
    if( compare_opencl_version(context->PLATFORM_VERSION, opencl_version_1_1) >= 0 )
      clGetContextInfo(context->context, CL_CONTEXT_NUM_DEVICES, sizeof(device_number), &device_number, NULL);
    else
#endif
    {
      size_t nContextDescriptorSize;
      clGetContextInfo(context->context, CL_CONTEXT_DEVICES, 0, 0, &nContextDescriptorSize);
      device_number = nContextDescriptorSize/sizeof(cl_device_id);
    }
    cl_device_id * aDevices = (cl_device_id *) malloc(sizeof(cl_device_id)*device_number);
    clGetContextInfo(context->context, CL_CONTEXT_DEVICES, sizeof(cl_device_id)*device_number, aDevices, 0);

    get_device_infos(aDevices[0], infos);

    free(aDevices);
}

static void get_platform_version(cl_platform_id platform_id, struct _opencl_version * version) {
    cl_int ciErrNum = CL_SUCCESS;
    size_t cl_platform_version_size;
    ciErrNum = clGetPlatformInfo(platform_id, CL_PLATFORM_VERSION, 0, NULL, &cl_platform_version_size);
    oclErrorCheck(ciErrNum,"Failed to get CL_PLATFORM_VERSION size!");
    char * cl_platform_version;
    cl_platform_version = (char *)malloc( cl_platform_version_size );
    if(cl_platform_version == NULL) {
      fprintf(stderr,"Error: Failed to create string (out of memory)!\n");
      exit(1);
    }
    ciErrNum = clGetPlatformInfo(platform_id, CL_PLATFORM_VERSION, cl_platform_version_size, cl_platform_version, NULL);
    oclErrorCheck(ciErrNum,"Failed to get CL_PLATFORM_VERSION!");
    //OpenCL<space><major_version.minor_version><space><platform-specific information>
    char minor[2], major[2];
    major[0] = cl_platform_version[7];
    major[1] = 0;
    minor[0] = cl_platform_version[9];
    minor[1] = 0;
    version->major = atoi( major );
    version->major = atoi( minor );
    free(cl_platform_version);
}

void liborbs_create_context(liborbs_context *context,
                            const char *platform, const char *devices,
                            cl_int device_type, cl_uint *device_number)
{
    cl_int ciErrNum = CL_SUCCESS;
    cl_platform_id *platform_ids;
    cl_uint num_platforms;
    clGetPlatformIDs(0, NULL, &num_platforms);
    //printf("num_platforms: %d\n",num_platforms);
    if(num_platforms == 0) {
      fprintf(stderr,"No OpenCL platform available!\n");
      return;
    }
    platform_ids = (cl_platform_id *)malloc(num_platforms * sizeof(cl_platform_id));
    clGetPlatformIDs(num_platforms, platform_ids, NULL);
    cl_context_properties properties[] = {CL_CONTEXT_PLATFORM, 0, 0 };
    if(strlen(platform)) {
      cl_uint found = 0;
      cl_uint i;
      for(i=0; i<num_platforms; i++){
        size_t info_length;
        char * info;
        clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, 0, NULL, &info_length);
        info = (char *)malloc(info_length * sizeof(char));
        clGetPlatformInfo(platform_ids[i], CL_PLATFORM_VENDOR, info_length, info, NULL);
        if(strcasestr(info, platform)){
          properties[1] = (cl_context_properties) platform_ids[i];
          found = 1;
          free(info);
          break;
        }
        free(info);
        clGetPlatformInfo(platform_ids[i], CL_PLATFORM_NAME, 0, NULL, &info_length);
        info = (char *)malloc(info_length * sizeof(char));
        clGetPlatformInfo(platform_ids[i], CL_PLATFORM_NAME, info_length, info, NULL);
        if(strcasestr(info, platform)){
          properties[1] = (cl_context_properties) platform_ids[i];
          found = 1;
          free(info);
          break;
        }
        free(info);
      }
      if(!found) {
        fprintf(stderr, "No matching OpenCL platform available : %s!\n", platform);
        return;
      }
    } else {
      properties[1] = (cl_context_properties) platform_ids[0];
    }

    cl_device_type type;
    if(device_type == 2) {
      type = CL_DEVICE_TYPE_GPU;
    } else if(device_type == 3) {
      type = CL_DEVICE_TYPE_CPU;
    } else if(device_type == 4) {
      type = CL_DEVICE_TYPE_ACCELERATOR;
    } else {
      type = CL_DEVICE_TYPE_ALL;
    }
    if(strlen(devices)) {
      cl_uint found = 0;
      cl_uint i;
      cl_uint num_devices;
      cl_device_id *device_ids;
      cl_device_id *matching_device_ids;
      clGetDeviceIDs((cl_platform_id)properties[1], type, 0, NULL, &num_devices);
      if(num_devices == 0) {
        fprintf(stderr,"No device of type %d!\n", (int)type);
        return;
      }
      device_ids = (cl_device_id *)malloc(num_devices * sizeof(cl_device_id));
      matching_device_ids = (cl_device_id *)malloc(num_devices * sizeof(cl_device_id));
      clGetDeviceIDs((cl_platform_id)properties[1], type, num_devices, device_ids, NULL);
      for(i=0; i<num_devices; i++){
        size_t info_length;
        char * info;
        clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, 0, NULL, &info_length);
        info = (char *)malloc(info_length * sizeof(char));
        clGetDeviceInfo(device_ids[i], CL_DEVICE_NAME, info_length, info, NULL);
        if(strcasestr(info, devices)) {
          matching_device_ids[found] = device_ids[i];
          found++;
        }
        free(info);
      }
      if(!found) {
        fprintf(stderr, "No matching OpenCL device available : %s!\n", devices);
        return;
      }
      context->context = clCreateContext(properties, found, matching_device_ids, NULL, NULL, &ciErrNum);
      free(matching_device_ids);
      free(device_ids);
    } else {
      context->context = clCreateContextFromType( properties , type, NULL, NULL, &ciErrNum);
    }
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("context address: %p\n", context);
#endif
    oclErrorCheck(ciErrNum,"Failed to create context!");
    
    get_platform_version((cl_platform_id)properties[1], &context->PLATFORM_VERSION);
    //getting the number of devices available in the context (devices which are of DEVICE_TYPE_GPU of platform platform_ids[0])
#ifdef CL_VERSION_1_1
    if( compare_opencl_version(context->PLATFORM_VERSION, opencl_version_1_1) >= 0 )
      clGetContextInfo(context->context, CL_CONTEXT_NUM_DEVICES, sizeof(*device_number), device_number, NULL);
    else
#endif
    {
      size_t nContextDescriptorSize;
      clGetContextInfo(context->context, CL_CONTEXT_DEVICES, 0, 0, &nContextDescriptorSize);
      *device_number = nContextDescriptorSize/sizeof(cl_device_id);
    }
    free( platform_ids );
    //printf("num_devices: %d\n",*device_number);
}
void FC_FUNC_(f_create_context, F_CREATE_CONTEXT)
    (liborbs_context **context, const char *platform, const char *devices,
     cl_int *device_type, cl_uint *device_number)
{
    *context = (liborbs_context*)malloc(sizeof(liborbs_context));
    if(*context == NULL) {
      fprintf(stderr,"Error: Failed to create context (out of memory)!\n");
      exit(1);
    }
    liborbs_create_context(*context, platform, devices, *device_type, device_number);
}

void liborbs_create_command_queue(liborbs_command_queue *command_queue,
                                  liborbs_context *context, cl_uint index)
{
    cl_int ciErrNum;
    cl_uint device_number;
#ifdef CL_VERSION_1_1
    if( compare_opencl_version(context->PLATFORM_VERSION, opencl_version_1_1) >= 0 )
      clGetContextInfo(context->context, CL_CONTEXT_NUM_DEVICES, sizeof(device_number), &device_number, NULL);
    else
#endif
    {
      size_t nContextDescriptorSize; 
      clGetContextInfo(context->context, CL_CONTEXT_DEVICES, 0, 0, &nContextDescriptorSize);
      device_number = nContextDescriptorSize/sizeof(cl_device_id);
    }
    cl_device_id * aDevices = (cl_device_id *) malloc(sizeof(cl_device_id)*device_number);
    clGetContextInfo(context->context, CL_CONTEXT_DEVICES, sizeof(cl_device_id)*device_number, aDevices, 0);

#if PROFILING
    command_queue->command_queue =
        clCreateCommandQueue(context->context, aDevices[index % device_number], CL_QUEUE_PROFILING_ENABLE, &ciErrNum);
#else
    command_queue->command_queue =
        clCreateCommandQueue(context->context, aDevices[index % device_number], 0, &ciErrNum);
#endif
    oclErrorCheck(ciErrNum,"Failed to create command queue!");
    command_queue->PLATFORM_VERSION = context->PLATFORM_VERSION;
    get_device_infos(aDevices[index % device_number], &command_queue->device_infos);
    free(aDevices);
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, command queue: %p\n",
           context, command_queue->command_queue);
#endif
    command_queue->context = context;

    create_initialize_kernels(context, &command_queue->kernels);
    create_wavelet_kernels(context, &command_queue->kernels);
    create_uncompress_kernels(context, &command_queue->kernels);
    create_reduction_kernels(context, &command_queue->kernels);
    create_magicfilter_kernels(context, &command_queue->kernels);
    create_kinetic_kernels(context, &command_queue->kernels);
}
void FC_FUNC_(f_create_command_queue,F_CREATE_COMMAND_QUEUE)
    (liborbs_command_queue **command_queue, liborbs_context *context, cl_uint *index)
{
    *command_queue = (liborbs_command_queue*)malloc(sizeof(liborbs_command_queue));
    if(*command_queue == NULL) {
      fprintf(stderr,"Error: Failed to create command queue (out of memory)!\n");
      exit(1);
    }
    liborbs_create_command_queue(*command_queue, context, *index);
}

void liborbs_clean_command_queue(liborbs_command_queue *command_queue)
{
  clean_initialize_kernels(&command_queue->kernels);
  clean_magicfilter_kernels(&command_queue->kernels);
  clean_wavelet_kernels(&command_queue->kernels);
  clean_uncompress_kernels(&command_queue->kernels);
  clean_reduction_kernels(&command_queue->kernels);
  clean_kinetic_kernels(&command_queue->kernels);
  clReleaseCommandQueue(command_queue->command_queue);
}
void FC_FUNC_(f_clean_command_queue,F_CLEAN_COMMAND_QUEUE)
    (liborbs_command_queue **command_queue)
{
  if (*command_queue) {
    liborbs_clean_command_queue(*command_queue);
    free(*command_queue);
    *command_queue = NULL;
  }
}

void liborbs_build_programs(liborbs_context *context)
{
    build_magicfilter_programs(context);
    build_wavelet_programs(context);
    build_uncompress_programs(context);
    build_initialize_programs(context);
    build_reduction_programs(context);
    build_kinetic_programs(context);
}
void FC_FUNC_(f_build_programs,F_BUILD_PROGRAMS)(liborbs_context **context)
{
    liborbs_build_programs(*context);
}

void liborbs_clean_context(liborbs_context *context)
{
  clean_magicfilter_programs(context);
  clean_initialize_programs(context);
  clean_wavelet_programs(context);
  clean_uncompress_programs(context);
  clean_reduction_programs(context);
  clean_kinetic_programs(context);
  clReleaseContext(context->context);
}
void FC_FUNC_(f_clean_context,F_CLEAN_CONTEXT)(liborbs_context **context)
{
    liborbs_clean_context(*context);
    free(*context);
}

void FC_FUNC_(ocl_release_mem_object,OCL_RELEASE_MEM_OBJECT)(cl_mem *buff_ptr) {
    cl_int ciErrNum = clReleaseMemObject( *buff_ptr);
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("memory address: %p\n",*buff_ptr);
#endif
    oclErrorCheck(ciErrNum,"Failed to release buffer!");
}

/** Creates a OpenCL read only buffer.
 *  @param context where the buffer is created.
 *  @param size of the buffer.
 *  @param buff_ptr return value : a buffer object reference.
 */
void FC_FUNC_(ocl_create_read_buffer,OCL_CREATE_READ_BUFFER)(liborbs_context **context, cl_uint *size, cl_mem *buff_ptr) {
    cl_int ciErrNum = CL_SUCCESS;
    *buff_ptr = clCreateBuffer( (*context)->context, CL_MEM_READ_ONLY, *size, NULL, &ciErrNum);
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to create read buffer!");
}

/** Copies data from Host memory to an OpenCL buffer.
 *  @param command_queue a pointer to the command queue used to make the copy.
 *  @param buffer to copy data to.
 *  @param size of the data to copy.
 *  @param host_ptr to copy the data from.
 */
void FC_FUNC_(ocl_enqueue_write_buffer,OCL_ENQUEUE_WRITE_BUFFER)(liborbs_command_queue **command_queue, cl_mem *buffer, cl_uint *size, const void *ptr){
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("command queue: %p, memory address: %p, size: %lu, source: %p\n",
           (*command_queue)->command_queue,*buffer,(long unsigned)*size, ptr);
#endif
    cl_int ciErrNum = clEnqueueWriteBuffer( (*command_queue)->command_queue, *buffer, CL_TRUE, 0, *size, ptr, 0, NULL, NULL);
    oclErrorCheck(ciErrNum,"Failed to enqueue write buffer!");
}

void FC_FUNC_(ocl_pin_read_buffer_async,OCL_PIN_READ_BUFFER_ASYNC)(liborbs_command_queue **command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*command_queue)->context->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);
    
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",(*command_queue)->context->context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin read buffer!");

    clEnqueueMapBuffer((*command_queue)->command_queue, *buff_ptr, CL_FALSE, CL_MAP_WRITE, 0, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer (async)!");
}

/** Creates an OpenCL buffer.
 *  @param context where the buffer is created.
 *  @param size of the buffer.
 *  @param buff_ptr return value : a buffer object reference.
 */
void FC_FUNC_(ocl_create_read_write_buffer,OCL_CREATE_READ_WRITE_BUFFER)(liborbs_context **context, cl_uint *size, cl_mem *buff_ptr) {
    cl_int ciErrNum = CL_SUCCESS;
    *buff_ptr = clCreateBuffer( (*context)->context, CL_MEM_READ_WRITE, *size, NULL, &ciErrNum);
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to create read_write buffer!");
}

/** Copies data from Host memory to an OpenCL buffer asynchronously.
 *  @param command_queue a pointer to the command queue used to make the copy.
 *  @param buffer to copy data to.
 *  @param size of the data to copy.
 *  @param host_ptr to copy the data from.
 */
void FC_FUNC_(ocl_enqueue_write_buffer_async,OCL_ENQUEUE_WRITE_BUFFER_ASYNC)(liborbs_command_queue **command_queue, cl_mem *buffer, cl_uint *size, const void *ptr){
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("command queue: %p, memory address: %p, size: %lu, source: %p\n",(*command_queue)->command_queue,*buffer,(long unsigned)*size, ptr);
#endif
    cl_int ciErrNum = clEnqueueWriteBuffer( (*command_queue)->command_queue, *buffer, CL_FALSE, 0, *size, ptr, 0, NULL, NULL);
    oclErrorCheck(ciErrNum,"Failed to enqueue write buffer!");
}

/** Copies data from an OpenCL buffer to Host memory.
 *  @param command_queue a pointer to the command queue used to make the copy.
 *  @param buffer to copy data from.
 *  @param size of the data to copy.
 *  @param host_ptr to copy the data to.
 */
void FC_FUNC_(ocl_enqueue_read_buffer,OCL_ENQUEUE_READ_BUFFER)(liborbs_command_queue **command_queue, cl_mem *buffer, cl_uint *size, void *ptr){
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("command queue: %p, memory address: %p, size: %lu, target: %p\n",(*command_queue)->command_queue,*buffer,(long unsigned)*size, ptr);
#endif
    cl_int ciErrNum = clEnqueueReadBuffer( (*command_queue)->command_queue, *buffer, CL_TRUE, 0, *size, ptr, 0, NULL, NULL);
    oclErrorCheck(ciErrNum,"Failed to enqueue read buffer!");
}

/** Copies data from an OpenCL buffer to Host memory asynchronously.
 *  @param command_queue a pointer to the command queue used to make the copy.
 *  @param buffer to copy data from.
 *  @param size of the data to copy.
 *  @param host_ptr to copy the data to.
 */
void FC_FUNC_(ocl_enqueue_read_buffer_async,OCL_ENQUEUE_READ_BUFFER_ASYNC)(liborbs_command_queue **command_queue, cl_mem *buffer, cl_uint *size, void *ptr){
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("command queue: %p, memory address: %p, size: %lu, target: %p\n",(*command_queue)->command_queue,*buffer,(long unsigned)*size, ptr);
#endif
    cl_int ciErrNum = clEnqueueReadBuffer( (*command_queue)->command_queue, *buffer, CL_FALSE, 0, *size, ptr, 0, NULL, NULL);
    oclErrorCheck(ciErrNum,"Failed to enqueue read buffer!");
}

void FC_FUNC_(ocl_unmap_mem_object,OCL_UNMAP_MEM_OBJECT)(liborbs_command_queue **command_queue, cl_mem *buff_ptr, void *host_ptr) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueUnmapMemObject((*command_queue)->command_queue, *buff_ptr, host_ptr, 0, NULL, NULL);
    oclErrorCheck(ciErrNum,"Failed to unmap pinned buffer!");
}

void FC_FUNC_(ocl_map_read_buffer_async,OCL_MAP_READ_BUFFER_ASYNC)(liborbs_command_queue **command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueMapBuffer((*command_queue)->command_queue, *buff_ptr, CL_FALSE, CL_MAP_READ , *offset, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer!");
}

void FC_FUNC_(ocl_pin_read_buffer,OCL_PIN_READ_BUFFER)(liborbs_context **context, liborbs_command_queue **command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);
    
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("context address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin read buffer!");

    clEnqueueMapBuffer((*command_queue)->command_queue, *buff_ptr, CL_TRUE, CL_MAP_WRITE , 0, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer!");
}

void FC_FUNC_(ocl_map_write_buffer_async,OCL_MAP_WRITE_BUFFER_ASYNC)(liborbs_command_queue **command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueMapBuffer((*command_queue)->command_queue, *buff_ptr, CL_FALSE, CL_MAP_WRITE , *offset, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned write buffer!");
}
