module wavelet_function_base
  use f_precision, wp => f_double
  implicit none

  private

  type, public :: wavelet_function
    ! wavelets plus scaling functions coefficients
    real(wp), dimension(:), pointer :: f_real => null()    !dpsi
    real(wp), dimension(:), pointer :: f_imag => null()    !ipsi
    real(wp), dimension(:,:), pointer :: f_cmplx => null() !zpsi
    type(locreg_descriptors) !keys, ndimension, number_of_resolutions, nvctr_total, nvctr(:), wavelet_family
  end type wavelet_function

end module wavelet_function_base
