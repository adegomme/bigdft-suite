!> @file
!!  Routines of compression and uncompression of the wavefunctions
!! @author
!!    Copyright (C) 2010-2011 BigDFT group 
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 

!!$subroutine fill_random(n1,n2,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,  & !n(c) mvctr_c, mvctr_f (arg:11,15)
!!$     mseg_c,keyg_c,  &
!!$     mseg_f,keyg_f,  & 
!!$     psig_c,psig_f)
!!$  use module_base
!!$  implicit none
!!$  integer, intent(in) :: n1,n2,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,mseg_c,mseg_f !n(c) mvctr_c,mvctr_f 
!!$  integer, dimension(2,mseg_c), intent(in) :: keyg_c
!!$  integer, dimension(2,mseg_f), intent(in) :: keyg_f
!!$  real(wp), dimension(0:n1,0:n2,0:n3), intent(out) :: psig_c
!!$  real(wp), dimension(7,nfl1:nfu1,nfl2:nfu2,nfl3:nfu3), intent(out) :: psig_f
!!$  !local variables
!!$  real(wp)::x
!!$  integer :: iseg,j0,j1,ii,i1,i2,i3,i0,i,l !n(c) jj
!!$
!!$  psig_c=0._wp
!!$  psig_f=0._wp
!!$  ! coarse part
!!$  do iseg=1,mseg_c
!!$     !n(c) jj=keyv_c(iseg)
!!$     j0=keyg_c(1,iseg)
!!$     j1=keyg_c(2,iseg)
!!$     ii=j0-1
!!$     i3=ii/((n1+1)*(n2+1))
!!$     ii=ii-i3*(n1+1)*(n2+1)
!!$     i2=ii/(n1+1)
!!$     i0=ii-i2*(n1+1)
!!$     i1=i0+j1-j0
!!$     do i=i0,i1
!!$      call random_number(x) 
!!$        psig_c(i,i2,i3)=x
!!$     enddo
!!$  enddo
!!$
!!$  ! fine part
!!$  do iseg=1,mseg_f
!!$     !n(c) jj=keyv_f(iseg)
!!$     j0=keyg_f(1,iseg)
!!$     j1=keyg_f(2,iseg)
!!$     ii=j0-1
!!$     i3=ii/((n1+1)*(n2+1))
!!$     ii=ii-i3*(n1+1)*(n2+1)
!!$     i2=ii/(n1+1)
!!$     i0=ii-i2*(n1+1)
!!$     i1=i0+j1-j0
!!$     do i=i0,i1
!!$       do l=1,7
!!$          call random_number(x)
!!$          psig_f(l,i,i2,i3)=x
!!$       enddo
!!$     enddo
!!$  enddo
!!$
!!$END SUBROUTINE fill_random


!> Compresses a wavefunction that is given in terms of fine scaling functions (psifscf) into 
!! the retained coarse scaling functions and wavelet coefficients (psi_c,psi_f)
subroutine compress_per_scal(wfd, nc, meshf, psifscf, psi, scal, psig)
  use liborbs_precisions
  use compression
  use box
  implicit none
  type(wavefunctions_descriptors), intent(in) :: wfd
  integer, dimension(2, 3), intent(in) :: nc
  type(cell), intent(in) :: meshf
  real(wp), dimension(meshf%ndim), intent(in) :: psifscf
  real(wp), dimension(wfd%nvctr_c + 7*wfd%nvctr_f), intent(out) :: psi
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(nc(1,1):nc(2,1),2, nc(1,2):nc(2,2),2, nc(1,3):nc(2,3),2), &
       intent(inout) :: psig
  !local variables
  integer :: n1, n2, n3

  n1 = nc(2,1)-nc(1,1)+1
  n2 = nc(2,2)-nc(1,2)+1
  n3 = nc(2,3)-nc(1,3)+1

  ! decompose wavelets into coarse scaling functions and wavelets
  call analyse_per_self(n1-1,n2-1,n3-1,psifscf,psig)

  call wfd_compress(wfd, n1, n2, n3, psig, psi, scal)
END SUBROUTINE compress_per_scal

!> Expands the compressed wavefunction in vector form (psi_c,psi_f) 
!! into fine scaling functions (psifscf)
subroutine uncompress_per_scal(wfd, nc, meshf, psifscf, psi, scal, psig)
  use liborbs_precisions
  use compression
  use box
  implicit none
  type(wavefunctions_descriptors), intent(in) :: wfd
  integer, dimension(2, 3), intent(in) :: nc
  type(cell), intent(in) :: meshf
  real(wp), dimension(meshf%ndim), intent(out) :: psifscf
  real(wp), dimension(wfd%nvctr_c + 7*wfd%nvctr_f), intent(in) :: psi
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(nc(1,1):nc(2,1),2, nc(1,2):nc(2,2),2, nc(1,3):nc(2,3),2), &
       intent(out) :: psig
  !local variables
  integer :: n1, n2, n3

  n1 = nc(2,1)-nc(1,1)+1
  n2 = nc(2,2)-nc(1,2)+1
  n3 = nc(2,3)-nc(1,3)+1
  call wfd_decompress(wfd, n1, n2, n3, psig, psi, scal)

  ! calculate fine scaling functions  
  call synthese_per_self(n1-1, n2-1, n3-1, psig, psifscf)

END SUBROUTINE uncompress_per_scal

!> Compresses a wavefunction that is given in terms of fine scaling functions (psifscf) into 
!! the retained coarse scaling functions and wavelet coefficients (psi_c,psi_f)
subroutine compress_slab_scal(wfd, nc, meshf, psifscf, psi, scal, psig)
  use liborbs_precisions
  use compression
  use box
  implicit none
  type(wavefunctions_descriptors), intent(in) :: wfd
  integer, dimension(2, 3), intent(in) :: nc
  type(cell), intent(in) :: meshf
  real(wp), dimension(meshf%ndim), intent(in) :: psifscf
  real(wp), dimension(wfd%nvctr_c + 7*wfd%nvctr_f), intent(out) :: psi
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(nc(1,1):nc(2,1),2, nc(1,2):nc(2,2),2, nc(1,3):nc(2,3),2), &
       intent(inout) :: psig
  !local variables
  integer :: n1, n2, n3

  n1 = nc(2,1)-nc(1,1)+1
  n2 = nc(2,2)-nc(1,2)+1
  n3 = nc(2,3)-nc(1,3)+1

  ! decompose wavelets into coarse scaling functions and wavelets
  call analyse_slab_self(n1-1,n2-1,n3-1,psifscf,psig)

  call wfd_compress(wfd, n1, n2, n3, psig, psi, scal)
END SUBROUTINE compress_slab_scal


!> Expands the compressed wavefunction in vector form (psi_c,psi_f) 
!! into fine scaling functions (psifscf)
subroutine uncompress_slab_scal(wfd, nc, meshf, psifscf, psi, scal, psig)
  use liborbs_precisions
  use compression
  use box
  implicit none
  type(wavefunctions_descriptors), intent(in) :: wfd
  integer, dimension(2, 3), intent(in) :: nc
  type(cell), intent(in) :: meshf
  real(wp), dimension(meshf%ndim), intent(out) :: psifscf
  real(wp), dimension(wfd%nvctr_c + 7*wfd%nvctr_f), intent(in) :: psi
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(nc(1,1):nc(2,1),2, nc(1,2):nc(2,2),2, nc(1,3):nc(2,3),2), &
       intent(out) :: psig
  !local variables
  integer :: n1, n2, n3

  n1 = nc(2,1)-nc(1,1)+1
  n2 = nc(2,2)-nc(1,2)+1
  n3 = nc(2,3)-nc(1,3)+1
  call wfd_decompress(wfd, n1, n2, n3, psig, psi, scal)

  ! calculate fine scaling functions  
  call synthese_slab_self(n1-1, n2-1, n3-1, psig, psifscf)

END SUBROUTINE uncompress_slab_scal


!> Compresses a wavefunction that is given in terms of fine scaling functions (psifscf) into 
!! the retained coarse scaling functions and wavelet coefficients (psi_c,psi_f)
subroutine compress_wire_scal(wfd, nc, meshf, psifscf, psi, scal, psig)
  use liborbs_precisions
  use compression
  use box
  implicit none
  type(wavefunctions_descriptors), intent(in) :: wfd
  integer, dimension(2, 3), intent(in) :: nc
  type(cell), intent(in) :: meshf
  real(wp), dimension(meshf%ndim), intent(in) :: psifscf
  real(wp), dimension(wfd%nvctr_c + 7*wfd%nvctr_f), intent(out) :: psi
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(nc(1,1):nc(2,1),2, nc(1,2):nc(2,2),2, nc(1,3):nc(2,3),2), &
       intent(inout) :: psig
  !local variables
  integer :: n1, n2, n3

  n1 = nc(2,1)-nc(1,1)+1
  n2 = nc(2,2)-nc(1,2)+1
  n3 = nc(2,3)-nc(1,3)+1

  ! decompose wavelets into coarse scaling functions and wavelets
  call analyse_wire_self(n1-1,n2-1,n3-1,psifscf,psig)

  call wfd_compress(wfd, n1, n2, n3, psig, psi, scal)
END SUBROUTINE compress_wire_scal


!> Expands the compressed wavefunction in vector form (psi_c,psi_f) 
!! into fine scaling functions (psifscf)
subroutine uncompress_wire_scal(wfd, nc, meshf, psifscf, psi, scal, psig)
  use liborbs_precisions
  use compression
  use box
  implicit none
  type(wavefunctions_descriptors), intent(in) :: wfd
  integer, dimension(2, 3), intent(in) :: nc
  type(cell), intent(in) :: meshf
  real(wp), dimension(meshf%ndim), intent(out) :: psifscf
  real(wp), dimension(wfd%nvctr_c + 7*wfd%nvctr_f), intent(in) :: psi
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(nc(1,1):nc(2,1),2, nc(1,2):nc(2,2),2, nc(1,3):nc(2,3),2), &
       intent(out) :: psig
  !local variables
  integer :: n1, n2, n3

  n1 = nc(2,1)-nc(1,1)+1
  n2 = nc(2,2)-nc(1,2)+1
  n3 = nc(2,3)-nc(1,3)+1
  call wfd_decompress(wfd, n1, n2, n3, psig, psi, scal)

  ! calculate fine scaling functions  
  call synthese_wire_self(n1,n2,n3,psig,psifscf)

END SUBROUTINE uncompress_wire_scal
