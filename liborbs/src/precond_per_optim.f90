!> @file
!!  Optimized routines to precondition wavefunctions
!! @author
!!    Copyright (C) 2010 BigDFT group 
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 

!>   Solves (KE+cprecr*I)*xx=yy by FFT in a cubic box 
!!   x_c is the right hand side on input and the solution on output
!!   This version uses work arrays kern_k1-kern_k3 and z allocated elsewhere
subroutine prec_fft_c(lr, &
     cprecr,hpsi,&
     kern_k1,kern_k2,kern_k3,z1,z3,x_c,&
     nd1,nd2,nd3,n1f,n1b,n3f,n3b,nd1f,nd1b,nd3f,nd3b,fac)
  use liborbs_precisions
  use compression
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  integer,intent(in) :: nd1,nd2,nd3
  integer,intent(in) :: n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b
  real(gp), intent(in) :: cprecr,fac
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi

  !work arrays
  real(gp) :: kern_k1(lr%mesh_coarse%ndims(1)),kern_k2(lr%mesh_coarse%ndims(2)),kern_k3(lr%mesh_coarse%ndims(3))
  real(wp), dimension(lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3)):: x_c! in and out of Fourier preconditioning
  real(wp) :: z1(2,nd1b,nd2,nd3,2)! work array
  real(wp) :: z3(2,nd1,nd2,nd3f,2)! work array

  call make_kernel(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%hgrids(1),kern_k1)
  call make_kernel(lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%hgrids(2),kern_k2)
  call make_kernel(lr%mesh_coarse%ndims(3)-1,lr%mesh_coarse%hgrids(3),kern_k3)

  call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

  call hit_with_kernel_fac(x_c,z1,z3,kern_k1,kern_k2,kern_k3,lr%mesh_coarse%ndims(1), &
       lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3),nd1,nd2,nd3,&
       n1f,n1b,n3f,n3b,nd1f,nd1b,nd3f,nd3b,cprecr,fac)

  call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

END SUBROUTINE prec_fft_c


!>   Solves (KE+cprecr*I)*xx=yy by FFT in a cubic box 
!!   x_c is the right hand side on input and the solution on output
!!   This version uses work arrays kern_k1-kern_k3 and z allocated elsewhere
subroutine prec_fft_fast(lr, cprecr, hpsi, &
     kern_k1,kern_k2,kern_k3,z1,z3,x_c, &
     nd1,nd2,nd3,n1f,n1b,n3f,n3b,nd1f,nd1b,nd3f,nd3b)
  use liborbs_precisions
  use compression
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  integer,intent(in)::nd1,nd2,nd3
  integer,intent(in)::n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi

  !work arrays
  real(gp):: kern_k1(lr%mesh_coarse%ndims(1)),kern_k2(lr%mesh_coarse%ndims(2)),kern_k3(lr%mesh_coarse%ndims(3))
  real(wp),dimension(lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3)):: x_c! in and out of Fourier preconditioning
  real(wp)::z1(2,nd1b,nd2,nd3,2)! work array
  real(wp)::z3(2,nd1,nd2,nd3f,2)! work array

  if (lr%wfd%nvctr_f > 0) then
     call wscal_f(lr%wfd%nvctr_f,hpsi(lr%wfd%nvctr_c+1),lr%mesh_coarse%hgrids(1), &
          lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3),cprecr)
  end if

  call make_kernel(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%hgrids(1),kern_k1)
  call make_kernel(lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%hgrids(2),kern_k2)
  call make_kernel(lr%mesh_coarse%ndims(3)-1,lr%mesh_coarse%hgrids(3),kern_k3)

  call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

  call  hit_with_kernel(x_c,z1,z3,kern_k1,kern_k2,kern_k3,lr%mesh_coarse%ndims(1), &
       lr%mesh_coarse%ndims(2),lr%mesh_coarse%ndims(3),nd1,nd2,nd3,&
       n1f,n1b,n3f,n3b,nd1f,nd1b,nd3f,nd3b,cprecr)

  call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3), x_c, hpsi)

END SUBROUTINE prec_fft_fast


!> Solves (KE+cprecr*I)*xx=yy by conjugate gradient method
!! hpsi is the right hand side on input and the solution on output
subroutine prec_fft(lr,cprecr,hpsi)
  use liborbs_precisions
  use dynamic_memory
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(inout) :: hpsi
  !local variables
  character(len=*), parameter :: subname='prec_fft'
  integer :: nd1,nd2,nd3
  real(gp), dimension(:), allocatable :: kern_k1,kern_k2,kern_k3
  real(wp), dimension(:,:,:), allocatable :: x_c! in and out of Fourier preconditioning
  real(wp), dimension(:,:,:,:,:), allocatable :: z1,z3 ! work array for FFT
  integer :: n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b

  ! Array sizes for the real-to-complex FFT: note that n1(there)=n1(here)+1
  ! and the same for n2,n3.
  call dimensions_fft(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
       nd1,nd2,nd3,n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b)

  call allocate_all

  call prec_fft_fast(lr, cprecr, hpsi, kern_k1, kern_k2, kern_k3, z1, z3, x_c, &
       nd1, nd2, nd3, n1f, n1b, n3f, n3b, nd1f, nd1b, nd3f, nd3b)

  call deallocate_all

contains

  subroutine allocate_all
    kern_k1 = f_malloc(lr%mesh_coarse%ndims(1),id='kern_k1')
    kern_k2 = f_malloc(lr%mesh_coarse%ndims(2),id='kern_k2')
    kern_k3 = f_malloc(lr%mesh_coarse%ndims(3),id='kern_k3')
    z1 = f_malloc((/ 2, nd1b, nd2, nd3, 2 /),id='z1')
    z3 = f_malloc((/ 2, nd1, nd2, nd3f, 2 /),id='z3')
    x_c = f_malloc(lr%mesh_coarse%ndims,id='x_c')
  END SUBROUTINE allocate_all

  subroutine deallocate_all
    call f_free(z1)
    call f_free(z3)
    call f_free(kern_k1)
    call f_free(kern_k2)
    call f_free(kern_k3)
    call f_free(x_c)
  END SUBROUTINE deallocate_all

END SUBROUTINE prec_fft


!> Applies the operator (KE+cprecr*I)*x=y
!! array x is input, array y is output
subroutine apply_hp_slab_k(lr, cprecr, kx, ky, kz, x, y, psifscf, ww, scal)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr,kx,ky,kz
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(array_dim(lr),2), intent(in) :: x
  real(wp), dimension(array_dim(lr),2), intent(out) :: y
  real(wp), dimension(lr%mesh_fine%ndim,2) :: ww,psifscf
  !local variables
  integer :: idx

  ! x: input, ww:work
  ! psifscf: output
  do idx=1,2
     call uncompress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, psifscf(1,idx), x(1,idx), scal, ww(1,idx))
  end do

  !transpose (to be included in the uncompression)
  call transpose_for_kpoints(2,lr%mesh_fine%ndims(1),lr%mesh_fine%ndims(2),lr%mesh_fine%ndims(3), &
       psifscf,ww,.true.)

  ! psifscf: input, ww: output
  call convolut_kinetic_slab_c_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
       lr%mesh_fine%hgrids,psifscf,ww,cprecr,kx,ky,kz)

  call transpose_for_kpoints(2,lr%mesh_fine%ndims(1),lr%mesh_fine%ndims(2),lr%mesh_fine%ndims(3),&
       ww,psifscf,.false.)

  ! ww:intput, psifscf: work
  ! y:output
  do idx=1,2
     call compress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, ww(1,idx), y(1,idx), scal, psifscf)
  end do

END SUBROUTINE apply_hp_slab_k


!> Applies the operator (KE+cprecr*I)*x=y
!! array x is input, array y is output
subroutine apply_hp_wire_k(lr, cprecr, kx, ky, kz, x, y, psifscf, ww, scal)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr,kx,ky,kz
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(array_dim(lr),2), intent(in) :: x
  real(wp), dimension(array_dim(lr),2), intent(out) :: y
  real(wp), dimension(lr%mesh_fine%ndim,2) :: ww,psifscf
  !local variables
  integer :: idx

  ! x: input, ww:work
  ! psifscf: output
  do idx=1,2
     call uncompress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, psifscf(1,idx), x(1,idx), scal, ww(1,idx))
  end do

  !transpose (to be included in the uncompression)
  call transpose_for_kpoints(2,lr%mesh_fine%ndims(1),lr%mesh_fine%ndims(2),lr%mesh_fine%ndims(3), &
       psifscf,ww,.true.)

  ! psifscf: input, ww: output
  call convolut_kinetic_wire_c_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
       lr%mesh_fine%hgrids,psifscf,ww,cprecr,kx,ky,kz)

  call transpose_for_kpoints(2,lr%mesh_fine%ndims(1),lr%mesh_fine%ndims(2),lr%mesh_fine%ndims(3),&
       ww,psifscf,.false.)

  ! ww:intput, psifscf: work
  ! y:output
  do idx=1,2
     call compress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, ww(1,idx), y(1,idx), scal, psifscf)
  end do

END SUBROUTINE apply_hp_wire_k

!>   Applies the operator (KE+cprecr*I)*x=y
!!   array x is input, array y is output
subroutine apply_hp_wire_sd(lr,cprecr,x,y,psifscf,ww,modul3,a,b,c,e)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(in) :: x
  real(wp), dimension(array_dim(lr)), intent(out) :: y
  integer, parameter :: lowfil=-14,lupfil=14
  real(wp),dimension(lr%mesh_fine%ndim)::ww,psifscf
  integer,intent(in)::modul3(lowfil:lr%mesh_coarse%ndims(3)+lupfil-1)
  real(gp),intent(in)::a(lowfil:lupfil,3)
  real(gp),intent(in)::b(lowfil:lupfil,3)
  real(gp),intent(in)::c(lowfil:lupfil,3)
  real(gp),intent(in)::e(lowfil:lupfil,3)

  real(wp), dimension(0:7), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp /)

  call apply_hp_wire_sd_scal(lr,cprecr,x,y,psifscf,ww,modul3,a,b,c,e,scal)
END SUBROUTINE apply_hp_wire_sd


!>   Applies the operator (KE+cprecr*I)*x=y
!!   array x is input, array y is output
subroutine apply_hp_wire_sd_scal(lr,cprecr,x,y,psifscf,ww,modul3,a,b,c,e,scal)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp),intent(in) :: scal(0:7)
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(in) :: x
  real(wp), dimension(array_dim(lr)), intent(out) :: y
  integer, parameter :: lowfil=-14,lupfil=14
  real(wp),dimension(lr%mesh_fine%ndim)::ww,psifscf
  integer,intent(in)::modul3(lowfil:lr%mesh_coarse%ndims(3)+lupfil-1)
  real(gp),intent(in)::a(lowfil:lupfil,3)
  real(gp),intent(in)::b(lowfil:lupfil,3)
  real(gp),intent(in)::c(lowfil:lupfil,3)
  real(gp),intent(in)::e(lowfil:lupfil,3)
  ! x: input
  ! psifscf: output
  call decompress_sd_scal(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
       x, psifscf, scal)

  ! psifscf: input, ww: output
  call convolut_kinetic_wire_sdc(lr%mesh_coarse%ndims(1)-1, lr%mesh_coarse%ndims(2)-1, &
       lr%mesh_coarse%ndims(3)-1,psifscf,ww,cprecr,modul3,a,b,c,e)
  ! ww:intput
  ! y:output
  call compress_sd_scal(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
       ww, y, scal)

END SUBROUTINE apply_hp_wire_sd_scal

!> Applies the operator (KE+cprecr*I)*x=y
!! array x is input, array y is output
subroutine apply_hp_per_k(lr, cprecr, kx, ky, kz, x, y, psifscf, ww, scal)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr,kx,ky,kz
  real(wp), dimension(0:7), intent(in) :: scal
  real(wp), dimension(array_dim(lr),2), intent(in) :: x
  real(wp), dimension(array_dim(lr),2), intent(out) :: y
  real(wp), dimension(lr%mesh_fine%ndim,2) :: ww,psifscf
  !local variables
  integer :: idx
  logical, parameter :: transpose = .false.

  ! x: input, ww:work
  ! psifscf: output
  do idx=1,2
     call uncompress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, psifscf, x(1,idx), scal, ww(1,idx))
  end do

  if (transpose) then
     !transpose (to be included in the uncompression)
     call transpose_for_kpoints(2,lr%mesh_fine%ndims(1),lr%mesh_fine%ndims(2),lr%mesh_fine%ndims(3), &
          psifscf,ww,.true.)
     call convolut_kinetic_per_c_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
          lr%mesh_fine%hgrids,psifscf,ww,cprecr,kx,ky,kz)
     !call convolut_kinetic_per_c(2*n1+1,2*n2+1,2*n3+1,hgridh,psifscf,ww,cprecr)
     !this can be included in the compression
     call transpose_for_kpoints(2,lr%mesh_fine%ndims(1),lr%mesh_fine%ndims(2),lr%mesh_fine%ndims(3),&
          ww,psifscf,.false.)
  else
     call convolut_kinetic_per_c_k_notranspose(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
          lr%mesh_fine%hgrids,psifscf,ww,cprecr,kx,ky,kz)
  end if

  ! ww:intput, psifscf: work
  ! y:output
  do idx=1,2
     call compress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, ww(1,idx), y(1,idx), scal, psifscf)
  end do

END SUBROUTINE apply_hp_per_k

!> Applies the operator (KE+cprecr*I)*x=y
!! array x is input, array y is output
subroutine apply_hp_hyb(lr, cprecr, x, y, x_f, x_c, x_f1, x_f2, x_f3, y_f, y_c)
  use liborbs_precisions
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  real(gp), intent(in) :: cprecr
  real(wp), dimension(array_dim(lr)), intent(in) :: x
  real(wp), dimension(array_dim(lr)), intent(out) :: y
  !work arrays 
  real(wp), dimension(lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(2), &
       lr%mesh_coarse%ndims(3)), intent(inout) :: x_c, y_c
  real(wp), dimension(7,lr%nboxf(1,1):lr%nboxf(2,1),lr%nboxf(1,2):lr%nboxf(2,2), &
       lr%nboxf(1,3):lr%nboxf(2,3)), intent(inout) :: x_f, y_f
  real(wp), dimension((lr%nboxf(2,1)-lr%nboxf(1,1)+1)* &
       (lr%nboxf(2,2)-lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)) :: x_f1, x_f2, x_f3

  real(wp), dimension(0:3), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp /)

  call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, x_c, x_f, x, scal, x_f1, x_f2, x_f3)

  call convolut_kinetic_hyb_c(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
       lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3), &
       lr%mesh_coarse%hgrids,x_c,x_f,y_c,y_f,cprecr,x_f1,x_f2,x_f3, &
       lr%bounds%kb%ibyz_f, lr%bounds%kb%ibxz_f, lr%bounds%kb%ibxy_f)

  call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, y, y_c, y_f, scal)

END SUBROUTINE apply_hp_hyb

!> multiplies a wavefunction psi_c,psi_f (in vector form) with a scaling vector (scal)
subroutine wscal_f(mvctr_f,psi_f,hx,hy,hz,c)
  use liborbs_precisions
  implicit none
  integer,intent(in)::mvctr_f
  real(gp),intent(in)::c,hx,hy,hz

  real(wp)::psi_f(7,mvctr_f)
  real(gp)::scal(7),hh(3)
  !WAVELET AND SCALING FUNCTION SECOND DERIVATIVE FILTERS, diagonal elements
  real(gp),PARAMETER::B2=24.8758460293923314_gp,A2=3.55369228991319019_gp

  integer i

  hh(1)=.5_gp/hx**2
  hh(2)=.5_gp/hy**2
  hh(3)=.5_gp/hz**2

  scal(1)=1._gp/(b2*hh(1)+a2*hh(2)+a2*hh(3)+c)       !  2 1 1
  scal(2)=1._gp/(a2*hh(1)+b2*hh(2)+a2*hh(3)+c)       !  1 2 1
  scal(3)=1._gp/(b2*hh(1)+b2*hh(2)+a2*hh(3)+c)       !  2 2 1
  scal(4)=1._gp/(a2*hh(1)+a2*hh(2)+b2*hh(3)+c)       !  1 1 2
  scal(5)=1._gp/(b2*hh(1)+a2*hh(2)+b2*hh(3)+c)       !  2 1 2
  scal(6)=1._gp/(a2*hh(1)+b2*hh(2)+b2*hh(3)+c)       !  1 2 2
  scal(7)=1._gp/(b2*hh(1)+b2*hh(2)+b2*hh(3)+c)       !  2 2 2

  do i=1,mvctr_f
     psi_f(1,i)=psi_f(1,i)*scal(1)       !  2 1 1
     psi_f(2,i)=psi_f(2,i)*scal(2)       !  1 2 1
     psi_f(3,i)=psi_f(3,i)*scal(3)       !  2 2 1
     psi_f(4,i)=psi_f(4,i)*scal(4)       !  1 1 2
     psi_f(5,i)=psi_f(5,i)*scal(5)       !  2 1 2
     psi_f(6,i)=psi_f(6,i)*scal(6)       !  1 2 2
     psi_f(7,i)=psi_f(7,i)*scal(7)       !  2 2 2
  enddo

END SUBROUTINE wscal_f


!> multiplies a wavefunction psi_c,psi_f (in vector form) with a scaling vector (scal)
subroutine wscal_per_self(mvctr_c,mvctr_f,scal,psi_c,psi_f)
  use liborbs_precisions
  implicit none
  integer,intent(in)::mvctr_c,mvctr_f
  real(gp),intent(in)::scal(0:7)
  real(wp),intent(inout)::psi_c(mvctr_c),psi_f(7,mvctr_f)

  integer i

  do i=1,mvctr_c
     psi_c(i)=psi_c(i)*scal(0)           !  1 1 1
  enddo

  do i=1,mvctr_f
     psi_f(1,i)=psi_f(1,i)*scal(1)       !  2 1 1
     psi_f(2,i)=psi_f(2,i)*scal(2)       !  1 2 1
     psi_f(3,i)=psi_f(3,i)*scal(3)       !  2 2 1
     psi_f(4,i)=psi_f(4,i)*scal(4)       !  1 1 2
     psi_f(5,i)=psi_f(5,i)*scal(5)       !  2 1 2
     psi_f(6,i)=psi_f(6,i)*scal(6)       !  1 2 2
     psi_f(7,i)=psi_f(7,i)*scal(7)       !  2 2 2
  enddo

END SUBROUTINE wscal_per_self


!> multiplies a wavefunction psi_c,psi_f (in vector form) with a scaling vector (scal)
subroutine wscal_per(mvctr_c,mvctr_f,scal,psi_c_in,psi_f_in,psi_c_out,psi_f_out)
  use liborbs_precisions
  implicit none
  integer,intent(in)::mvctr_c,mvctr_f
  real(gp),intent(in)::scal(0:7)
  real(wp),intent(in)::psi_c_in(mvctr_c),psi_f_in(7,mvctr_f)
  real(wp),intent(out)::psi_c_out(mvctr_c),psi_f_out(7,mvctr_f)

  integer i

  do i=1,mvctr_c
     psi_c_out(i)=psi_c_in(i)*scal(0)           !  1 1 1
  enddo

  do i=1,mvctr_f
     psi_f_out(1,i)=psi_f_in(1,i)*scal(1)       !  2 1 1
     psi_f_out(2,i)=psi_f_in(2,i)*scal(2)       !  1 2 1
     psi_f_out(3,i)=psi_f_in(3,i)*scal(3)       !  2 2 1
     psi_f_out(4,i)=psi_f_in(4,i)*scal(4)       !  1 1 2
     psi_f_out(5,i)=psi_f_in(5,i)*scal(5)       !  2 1 2
     psi_f_out(6,i)=psi_f_in(6,i)*scal(6)       !  1 2 2
     psi_f_out(7,i)=psi_f_in(7,i)*scal(7)       !  2 2 2
  enddo

END SUBROUTINE wscal_per


!> initialization for the array scal in the subroutine wscal_per  
subroutine wscal_init_per(scal,hx,hy,hz,c)
  use liborbs_precisions
  implicit none
  real(wp), intent(in) :: c,hx,hy,hz
  real(wp), dimension(0:7), intent(out) :: scal
  !local variables
  real(wp), parameter :: b2=24.8758460293923314d0,a2=3.55369228991319019d0
  real(gp) :: hh(3)

  hh(1)=.5_wp/hx**2
  hh(2)=.5_wp/hy**2
  hh(3)=.5_wp/hz**2

  scal(0)=1._wp/sqrt(a2*hh(1)+a2*hh(2)+a2*hh(3)+c)       !  1 1 1
  scal(1)=1._wp/sqrt(b2*hh(1)+a2*hh(2)+a2*hh(3)+c)       !  2 1 1
  scal(2)=1._wp/sqrt(a2*hh(1)+b2*hh(2)+a2*hh(3)+c)       !  1 2 1
  scal(3)=1._wp/sqrt(b2*hh(1)+b2*hh(2)+a2*hh(3)+c)       !  2 2 1
  scal(4)=1._wp/sqrt(a2*hh(1)+a2*hh(2)+b2*hh(3)+c)       !  1 1 2
  scal(5)=1._wp/sqrt(b2*hh(1)+a2*hh(2)+b2*hh(3)+c)       !  2 1 2
  scal(6)=1._wp/sqrt(a2*hh(1)+b2*hh(2)+b2*hh(3)+c)       !  1 2 2
  scal(7)=1._wp/sqrt(b2*hh(1)+b2*hh(2)+b2*hh(3)+c)       !  2 2 2

END SUBROUTINE wscal_init_per
