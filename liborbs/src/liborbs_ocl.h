#ifndef LIBORBS_OCL_H
#define LIBORBS_OCL_H

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

struct liborbs_device_infos {
  cl_device_type DEVICE_TYPE;
  size_t MAX_WORK_GROUP_SIZE;
  cl_ulong LOCAL_MEM_SIZE;
  cl_uint MAX_COMPUTE_UNITS;
  char NAME[1024];
};

struct liborbs_kernels { 
  cl_kernel c_initialize_kernel_d;
  cl_kernel v_initialize_kernel_d;
  /* cl_kernel p_initialize_kernel_d; */
  cl_kernel kinetic1d_kernel_d;
  cl_kernel kinetic1d_f_kernel_d;
  cl_kernel kinetic_k1d_kernel_d;
  cl_kernel kinetic_k1d_kernel_d_2;
  cl_kernel kinetic_k1d_f_kernel_d_2;
  cl_kernel magicfilter1d_kernel_d;
  cl_kernel magicfilter1d_straight_kernel_d;
  cl_kernel magicfilter1d_block_kernel_d;
  cl_kernel magicfilter1d_den_kernel_d;
  cl_kernel magicfilter1d_pot_kernel_d;
  cl_kernel magicfilter1d_t_kernel_d;
  cl_kernel magicfiltershrink1d_kernel_d;
  cl_kernel magicfiltergrow1d_kernel_d;
  cl_kernel magicfiltergrow1d_den_kernel_d;
  cl_kernel magicfiltergrow1d_pot_kernel_d;
  cl_kernel reduction_kernel_d;
  cl_kernel reduction_dot_kernel_d;
  cl_kernel axpy_kernel_d;
  cl_kernel axpy_offset_kernel_d;
  cl_kernel scal_kernel_d;
  cl_kernel copy_kernel_d;
  cl_kernel dot_kernel_d;
  cl_kernel set_kernel_d;
  /* cl_kernel void_kernel; */
  cl_kernel uncompress_coarse_kernel_d;
  cl_kernel uncompress_fine_kernel_d;
  cl_kernel uncompress_scale_coarse_kernel_d;
  cl_kernel uncompress_scale_fine_kernel_d;
  cl_kernel compress_coarse_kernel_d;
  cl_kernel compress_fine_kernel_d;
  cl_kernel compress_scale_coarse_kernel_d;
  cl_kernel compress_scale_fine_kernel_d;
  cl_kernel scale_psi_fine_kernel_d;
  cl_kernel scale_psi_coarse_kernel_d;
  cl_kernel ana1d_kernel_d;
  cl_kernel ana1d_block_kernel_d;
  cl_kernel anashrink1d_kernel_d;
  cl_kernel syn1d_kernel_d;
  cl_kernel syngrow1d_kernel_d;
  cl_kernel gemm_kernel_d;
  cl_kernel gemm_volkov_kernel_d;
  cl_kernel gemm_kernel_d_tb;
  cl_kernel gemm_kernel_d_ta;
  cl_kernel gemm_kernel_d_tatb;
  cl_kernel gemm_block_kernel_d;
  cl_kernel gemmsy_kernel_d;
  cl_kernel gemm_kernel_z;
  cl_kernel gemm_kernel_z_tb;
  cl_kernel gemm_kernel_z_cb;
  cl_kernel gemm_kernel_z_ta;
  cl_kernel gemm_kernel_z_ca;
  cl_kernel gemm_kernel_z_tatb;
  cl_kernel gemm_kernel_z_catb;
  cl_kernel gemm_kernel_z_tacb;
  cl_kernel gemm_kernel_z_cacb;
  /* cl_kernel benchmark_flops_kernel_d; */
  /* cl_kernel benchmark_mops_kernel_d; */
  /* cl_kernel transpose_kernel_d; */
  /* cl_kernel notranspose_kernel_d; */
  /* cl_kernel fft_kernel_d0_d; */
  /* cl_kernel fft_kernel_d1_d; */
  /* cl_kernel fft_kernel_d2_d; */
  /* cl_kernel fft_kernel_d0_r2c_d; */
  /* cl_kernel fft_kernel_d1_r2c_d; */
  /* cl_kernel fft_kernel_d2_r2c_d; */
  /* cl_kernel fft_kernel_d0_r_d; */
  /* cl_kernel fft_kernel_d1_r_d; */
  /* cl_kernel fft_kernel_d2_r_d; */
  /* cl_kernel fft_kernel_d0_r_c2r_d; */
  /* cl_kernel fft_kernel_d1_r_c2r_d; */
  /* cl_kernel fft_kernel_d2_r_c2r_d; */
  /* cl_kernel fft_kernel_k_d0_d; */
  /* cl_kernel fft_kernel_k_d1_d; */
  /* cl_kernel fft_kernel_k_d2_d; */
  /* cl_kernel fft_kernel_d0_f_d; */
  /* cl_kernel fft_kernel_d1_f_d; */
  /* cl_kernel fft_kernel_d2_f_d; */
  /* cl_kernel fft_kernel_d0_r2c_f_d; */
  /* cl_kernel fft_kernel_d1_r2c_f_d; */
  /* cl_kernel fft_kernel_d2_r2c_f_d; */
  /* cl_kernel fft_kernel_d0_r_f_d; */
  /* cl_kernel fft_kernel_d1_r_f_d; */
  /* cl_kernel fft_kernel_d2_r_f_d; */
  /* cl_kernel fft_kernel_d0_r_c2r_f_d; */
  /* cl_kernel fft_kernel_d1_r_c2r_f_d; */
  /* cl_kernel fft_kernel_d2_r_c2r_f_d; */
  /* cl_kernel fft_kernel_k_d0_f_d; */
  /* cl_kernel fft_kernel_k_d1_f_d; */
  /* cl_kernel fft_kernel_k_d2_f_d; */
};

struct _opencl_version {
  cl_uint minor;
  cl_uint major;
}; 
extern struct _opencl_version opencl_version_1_0;
extern struct _opencl_version opencl_version_1_1;
extern struct _opencl_version opencl_version_1_2;

struct _liborbs_context {
  cl_context context;
  struct _opencl_version PLATFORM_VERSION;
  /* cl_program benchmarkProgram; */
  /* cl_program fftProgramd0; */
  /* cl_program fftProgramd1; */
  /* cl_program fftProgramd2; */
  cl_program initializeProgram;
  cl_program kineticProgram;
  cl_program kinetic_kProgram;
  cl_program magicfilterProgram;
  cl_program reductionProgram;
  cl_program dgemmProgram;
  cl_program uncompressProgram;
  cl_program compressProgram;
  cl_program anaProgram;
  cl_program synProgram;
  /* cl_mem cossind0; */
  /* cl_mem cossind1; */
  /* cl_mem cossind2; */
  /* cl_uint fft_size[3]; */
  /* event * event_list; */
  /* size_t event_number; */
  /* size_t event_allocated; */
};
typedef struct _liborbs_context liborbs_context;

struct _liborbs_command_queue {
  liborbs_context *context;
  struct liborbs_kernels kernels;
  struct liborbs_device_infos device_infos;
  cl_command_queue command_queue;
  struct _opencl_version PLATFORM_VERSION;
};
typedef struct _liborbs_command_queue liborbs_command_queue;

cl_int compare_opencl_version(struct _opencl_version v1, struct _opencl_version v2);
void get_context_devices_infos(liborbs_context * context, struct liborbs_device_infos * infos);

void liborbs_create_command_queue(liborbs_command_queue *command_queue,
                                  liborbs_context *context, cl_uint index);
void liborbs_clean_command_queue(liborbs_command_queue *command_queue);

void liborbs_create_context(liborbs_context *context,
                            const char *platform, const char *devices,
                            cl_int device_type, cl_uint *device_number);
void liborbs_build_programs(liborbs_context *context);
void liborbs_clean_context(liborbs_context *context);

#endif
