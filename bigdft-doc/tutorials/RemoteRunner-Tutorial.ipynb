{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b431006f",
   "metadata": {},
   "source": [
    "# Remote Runner\n",
    "This lesson will demonstrate use of the `RemoteRunner` and `RemoteDataset` classes which allow you to run workflows while offloading some of the housekeeping to PyBigDFT.\n",
    "\n",
    "## Advantages over SystemCalculator\n",
    "The primary advantage this interface has over an explicit SystemCalculator instance is the portability of a calculation. A notebook which runs your calculation can be easily transferred to and run on another machine.\n",
    "\n",
    "Additionally, this also allows a disconnect between your notebook and the calculations being run. For example, you could submit 200 calculations, and continue your work later on even if the notebook was closed.\n",
    "\n",
    "## Remote Runner Infrastructure\n",
    "First we construct the infastructure needed for remote runs. What we need to do is to instantiate a class named `RemoteRunner`. \n",
    "Such class needs a script template. This will generate the jobscript that you want to submit on the remote machine.\n",
    "Such a script template can be provided as a string or as a function which returns a string. In this second case the argument of this function can be\n",
    "used to specify the template script to a given run.\n",
    "Then, this class requires a python function, its arguments, and the url of the remote machine in which we would like to calculation to be done.\n",
    "\n",
    "We start this example with a script provided as a string. We will then specify the situation to a set of calculations and a set of scripts with dependencies."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb4f6a11",
   "metadata": {},
   "source": [
    "## Executing remotely\n",
    "\n",
    "This tutorial will cover RemoteRunner in \"local\" mode, this will allow you to explore the functionality of the runner on your machine, without having to tweak connection variables.\n",
    "\n",
    "However, everything can be executed on a remote machine to which we have ssh access to. Simply add the url command at the instantiation, or the run method of the same instance.\n",
    "\n",
    "Either: `R.RemoteRunner(..., url='user@123.456.78.90', ...)`\n",
    "\n",
    "Or: `remote_code.run(url='user@123.456.78.90')`\n",
    "\n",
    "Additionally, the `ssh` and `python` commands can be specified, to customise for your specific run conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "fad37e44",
   "metadata": {},
   "outputs": [],
   "source": [
    "import BigDFT.RemoteRunners as R"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0d6213d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "local_scr=\"\"\"#!/bin/bash\n",
    "export I_MPI_FABRICS=ofi\n",
    "# BigDFT Parallelism\n",
    "export BIGDFT_MPIRUN=\"mpirun -np NMPIVAR\"\n",
    "export OMP_NUM_THREADS=NOMPVAR\n",
    "\n",
    "# General Environment\n",
    "source BIGDFT_PREFIX/bin/bigdftvars.sh\n",
    "\n",
    "# Actual Run goes after this line\n",
    "\"\"\"\n",
    "\n",
    "# change this to your /path/to/build/install directory\n",
    "BIGDFT_PREFIX='/home/pi/build/install' "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2298aa19",
   "metadata": {},
   "source": [
    "There are keywords in this script that will be substituted with arguments. This will let you configure things on a per calculation run, such as the number of nodes used, number of openmp threads, etc. This can also be implemented more explicitly via a function with arguments which returns the substitutes script as a string. Now we are ready to instantiate an object of this class.\n",
    "\n",
    "## Calculation Examples\n",
    "Now let's try an example calculation. The remote runner can be used to run any snippet of python code. Let's try something very simple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "cd6805dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "def hello_world(argument):\n",
    "    from time import sleep\n",
    "    sleep(5)\n",
    "    return \"hello \" + argument"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "4036cecd",
   "metadata": {},
   "outputs": [],
   "source": [
    "remote_code = R.RemoteRunner(hello_world,\n",
    "                             arguments=dict(argument='BigDFT'),\n",
    "                             remote_dir='/tmp', \n",
    "                             script=local_scr,\n",
    "                             NMPIVAR=1,\n",
    "                             NOMPVAR=1,\n",
    "                             skip=False,\n",
    "                             BIGDFT_PREFIX=BIGDFT_PREFIX)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37eaa091",
   "metadata": {},
   "source": [
    "The run command is used to submit a job. \n",
    "\n",
    "As for the arguments to the run command, we can pass (not specified here): \n",
    "\n",
    "- a unique name for the calculation\n",
    "- a directory we want to run in\n",
    "- can also pass a dictionary of arguments that are passed to our custom `make_script` function.\n",
    "\n",
    "The optional `run_dir` helps prevent name clashes. Note that this directory will also be created locally for storing some temporary files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "92bbcf54",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "executing command: 'rsync -auv /tmp/hello_world-run.sh /tmp/hello_world-function-files.tar.gz /tmp/hello_world-function-run.py /tmp'\n",
      "sending incremental file list\n",
      "\n",
      "sent 116 bytes  received 12 bytes  256.00 bytes/sec\n",
      "total size is 2,160  speedup is 16.88\n",
      "\n",
      "executing command: 'cd /tmp && bash hello_world-run.sh'\n"
     ]
    }
   ],
   "source": [
    "remote_code.run(asynchronous=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9566f6d1",
   "metadata": {},
   "source": [
    "### Results\n",
    "\n",
    "What we need to do next is to wait for the calculation to complete. The runner has the functions we need in order to check if a calculation is done."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "00945514",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Checking for finished run... locally... Yes (Found a results file)\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "remote_code.is_finished()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "607aa666",
   "metadata": {},
   "source": [
    "`is_finished()` works by checking for a results file, returning `True` if one is found.\n",
    "\n",
    "If you are experimenting with this, you may have noticed behaviour indicating that `is_finished()` will return _any_ results file. To report only whether or not the _most recent_ run has finished, pass:\n",
    "\n",
    "`is_finished(anyfile=False)`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b1954d7",
   "metadata": {},
   "source": [
    "There are also optional arguments which may allow for further customisation of your workflow:\n",
    "\n",
    "`timeout`: Passing a positive integer _n_ here will allow `is_finished` to fail _n_ amount of times (default 0). Passing a negative number will allow for ignoring of errors. This can be useful, but dangerous within loops.\n",
    "\n",
    "`verbose`: Set to false to disable status outputs when checking for files."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "99d53dfa",
   "metadata": {},
   "source": [
    "The following loop will quietly wait until the most recent run has output a recent results file, then store the result in `return_str`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "8eadeae2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Checking for finished run... locally... Yes (Found a results file)\n",
      "executing command: 'rsync -auv /tmp/hello_world-function-result.json /tmp/hello_world-function-result.json /tmp/hello_world-function-result.json /tmp'\n",
      "sending incremental file list\n",
      "\n",
      "sent 100 bytes  received 12 bytes  224.00 bytes/sec\n",
      "total size is 42  speedup is 0.38\n",
      "\n",
      "hello BigDFT\n"
     ]
    }
   ],
   "source": [
    "import time\n",
    "while not remote_code.is_finished(anyfile=False, verbose=False): \n",
    "    time.sleep(1)\n",
    "return_str = remote_code.fetch_result()\n",
    "\n",
    "print(return_str)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6571e911",
   "metadata": {},
   "source": [
    "## BigDFT Calculation Dataset\n",
    "The above example shows a single run of a simple function. However there also exists `RemoteDataset`, which behaves similarly, but is more expansive.\n",
    "\n",
    "The goal of `RemoteDataset` is to facilitate exploration of your function within \"parameter-space\". This is done by instantating a Dataset, and appending runs to it with your parameters.\n",
    "\n",
    "`RemoteDataset` is written to be interfaced with as similarly to `RemoteRunner` as possible. This will allow a workflow prototyped with `RemoteRunner` to be easily expanded upon, and moved to a larger machine (thanks to the flexibility of the Remote systems)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11863d18",
   "metadata": {},
   "source": [
    "To illustrate the basic behaviour of this mode, we will write a function which invokes the system calculator and returns the logfile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "9c0ac0cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def run_calculation(inp):\n",
    "    from BigDFT.IO import XYZReader\n",
    "    from BigDFT.Systems import System\n",
    "    from BigDFT.Fragments import Fragment\n",
    "\n",
    "    sys = System()\n",
    "    with XYZReader(\"H2O\") as ifile:\n",
    "        sys[\"WAT:0\"] = Fragment(xyzfile=ifile)\n",
    "\n",
    "    from BigDFT.Calculators import SystemCalculator\n",
    "    code = SystemCalculator()\n",
    "    log = code.run(posinp=sys.get_posinp(), input=inp)\n",
    "    return log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cd05494",
   "metadata": {},
   "source": [
    "Now set up a RemoteDataset instance to handle the runners.\n",
    "\n",
    "When running in this mode, arguments are split across the RemoteDataset instance and the runs themselves. With globally relevant arguments being passed to the initial instantation, and then inherited by the runners (if required).\n",
    "\n",
    "For example, here we specify:\n",
    "\n",
    "`function`: The function to be run for each run, as we defined previously.\n",
    "\n",
    "`run_dir`: Local directory for file preparation\n",
    "\n",
    "`remote_dir`: Directory where the files will be copied to and run from\n",
    "\n",
    "`script`: Much like RemoteRunner, only each runner inherits this from the Dataset call, along with the substituted arguments that follow in this example\n",
    "\n",
    "You can also specify the argument `database_file` which is the name of a local .yaml file you will use to store the status of the remote database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "68b8ffdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "remote_dataset = R.RemoteDataset(function=run_calculation, \n",
    "                                 run_dir=\"examples/\",\n",
    "                                 remote_dir='examples/tmp', \n",
    "                                 protocol='Dill',\n",
    "                                 script=local_scr, \n",
    "                                 NMPIVAR=4,\n",
    "                                 NOMPVAR=1,\n",
    "                                 BIGDFT_PREFIX=BIGDFT_PREFIX)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f86a7174",
   "metadata": {},
   "source": [
    "Now we must append runs to this dataset for calculation. With `RemoteRunner`, the arguments were passed to the runner itself, however here we pass the (relevant local) arguments with an append function. This also allows us to specify other aspects of the run, such as the `id`.\n",
    "\n",
    "In this example, the `id` is simply a 1 dimensional value, but you can imagine if we were varying more than one parameter, the `id` suddenly becomes a nice way of identifying runs within your value-spread."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "bd8823e7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.4\n",
      "0.37\n",
      "0.35\n",
      "0.3\n"
     ]
    }
   ],
   "source": [
    "import BigDFT.RemoteRunners as R\n",
    "from BigDFT.Inputfiles import Inputfile\n",
    "from copy import deepcopy\n",
    "input_files = {}\n",
    "\n",
    "inp = Inputfile()\n",
    "inp.set_xc(\"PBE\")\n",
    "# generate arguments\n",
    "for cutoff in [\"0.4\", \"0.37\", \"0.35\", \"0.3\"]:\n",
    "    input_files[cutoff] = deepcopy(inp)\n",
    "    input_files[cutoff].set_hgrid(float(cutoff))\n",
    "# append them to the RemoteDataset\n",
    "for cutoff, inp in input_files.items():\n",
    "    print(cutoff)\n",
    "    remote_dataset.append_run(id={'h':cutoff}, arguments=dict(inp=inp))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85bb18ba",
   "metadata": {},
   "source": [
    "We now have the concept of a database which keeps track of the runs which have been submitted:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "906eb540",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'calc': <BigDFT.RemoteRunners.RemoteRunner at 0x7f1c14147af0>, 'runs': [0]},\n",
       " {'calc': <BigDFT.RemoteRunners.RemoteRunner at 0x7f1c14147b80>, 'runs': [1]},\n",
       " {'calc': <BigDFT.RemoteRunners.RemoteRunner at 0x7f1c1415a550>, 'runs': [2]},\n",
       " {'calc': <BigDFT.RemoteRunners.RemoteRunner at 0x7f1c1415ae80>, 'runs': [3]}]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "remote_dataset.calculators"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c03785c",
   "metadata": {},
   "source": [
    "### Run your calculations\n",
    "\n",
    "On run, RemoteDataset will copy all necessary files from `run_dir` to `remote_dir`, and run your calculations. The results of these calculations will appear as results files within that folder as they finish."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "4a6b96e1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "executing command: 'rsync -auv examples/h0.4-run.sh examples/h0.4-function-serialize.dill examples/h0.4-function-run.py examples/h0.37-run.sh examples/h0.37-function-serialize.dill examples/h0.37-function-run.py examples/h0.35-run.sh examples/h0.35-function-serialize.dill examples/h0.35-function-run.py examples/h0.3-run.sh examples/h0.3-function-serialize.dill examples/h0.3-function-run.py examples/tmp'\n",
      "sending incremental file list\n",
      "h0.3-function-run.py\n",
      "h0.3-function-serialize.dill\n",
      "h0.3-run.sh\n",
      "h0.35-function-run.py\n",
      "h0.35-function-serialize.dill\n",
      "h0.35-run.sh\n",
      "h0.37-function-run.py\n",
      "h0.37-function-serialize.dill\n",
      "h0.37-run.sh\n",
      "h0.4-function-run.py\n",
      "h0.4-function-serialize.dill\n",
      "h0.4-run.sh\n",
      "\n",
      "sent 14,596 bytes  received 244 bytes  29,680.00 bytes/sec\n",
      "total size is 13,758  speedup is 0.93\n",
      "\n",
      "executing command: 'cd examples/tmp && bash h0.4-run.sh'\n",
      "executing command: 'cd examples/tmp && bash h0.37-run.sh'\n",
      "executing command: 'cd examples/tmp && bash h0.35-run.sh'\n",
      "executing command: 'cd examples/tmp && bash h0.3-run.sh'\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "{0: None, 1: None, 2: None, 3: None}"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "remote_dataset.run(force=True, run_dir='examples/')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d2ac172",
   "metadata": {},
   "source": [
    "Results can be retrieved using `fetch_results()`\n",
    "\n",
    "Much like RemoteRunner, we can call `is_finished()` to ensure that we have results prior to retreival. This implementation returns `False` unless all submitted runs have returned a result, allowing you to block progress until meaningful results can be retrieved."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "a6d51c68",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Checking for finished run... locally... Yes (Found a results file)\n",
      "executing command: 'rsync -auv examples/tmp/h0.4-function-result.dill examples/tmp/h0.4-function-result.dill examples/tmp/h0.4-function-result.dill examples/'\n",
      "sending incremental file list\n",
      "h0.4-function-result.dill\n",
      "\n",
      "sent 24,461 bytes  received 35 bytes  48,992.00 bytes/sec\n",
      "total size is 72,963  speedup is 2.98\n",
      "\n",
      "Checking for finished run... locally... Yes (Found a results file)\n",
      "executing command: 'rsync -auv examples/tmp/h0.37-function-result.dill examples/tmp/h0.37-function-result.dill examples/tmp/h0.4-function-result.dill examples/tmp/h0.37-function-result.dill examples/'\n",
      "sending incremental file list\n",
      "h0.37-function-result.dill\n",
      "\n",
      "sent 24,521 bytes  received 35 bytes  49,112.00 bytes/sec\n",
      "total size is 97,284  speedup is 3.96\n",
      "\n",
      "Checking for finished run... locally... Yes (Found a results file)\n",
      "executing command: 'rsync -auv examples/tmp/h0.35-function-result.dill examples/tmp/h0.35-function-result.dill examples/tmp/h0.4-function-result.dill examples/tmp/h0.37-function-result.dill examples/tmp/h0.35-function-result.dill examples/'\n",
      "sending incremental file list\n",
      "h0.35-function-result.dill\n",
      "\n",
      "sent 24,552 bytes  received 35 bytes  49,174.00 bytes/sec\n",
      "total size is 121,605  speedup is 4.95\n",
      "\n",
      "Checking for finished run... locally... Yes (Found a results file)\n",
      "executing command: 'rsync -auv examples/tmp/h0.3-function-result.dill examples/tmp/h0.3-function-result.dill examples/tmp/h0.4-function-result.dill examples/tmp/h0.37-function-result.dill examples/tmp/h0.35-function-result.dill examples/tmp/h0.3-function-result.dill examples/'\n",
      "sending incremental file list\n",
      "h0.3-function-result.dill\n",
      "\n",
      "sent 24,586 bytes  received 35 bytes  49,242.00 bytes/sec\n",
      "total size is 145,926  speedup is 5.93\n",
      "\n"
     ]
    }
   ],
   "source": [
    "import time\n",
    "while not remote_dataset.is_finished(anyfile=False):\n",
    "    time.sleep(1)\n",
    "results = remote_dataset.fetch_results()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58ccbc6c",
   "metadata": {},
   "source": [
    "Results are returned as a list of the outputs returned from the remote function. We can then, for example, zip them into a dict together with their input arguments for further processing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "25f86245",
   "metadata": {},
   "outputs": [],
   "source": [
    "logfiles = {c:v for c, v in zip(input_files,results)}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "0ae27508",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "76"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "logfiles['0.4'].memory_peak"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
