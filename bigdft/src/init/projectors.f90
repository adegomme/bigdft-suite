!> @file
!!  Routines to handle projectors
!! @author
!!    Copyright (C) 2010-2015 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS

!> Returns the compressed form of a Gaussian projector
!! @f$ x^lx * y^ly * z^lz * exp (-1/(2*gau_a^2) *((x-rx)^2 + (y-ry)^2 + (z-rz)^2 )) @f$
!! in the array proj.
subroutine crtproj(kx,ky,kz,ncplx_k,g,rx,ry,rz,lr,proj,wpr,gau_cut)
  use module_precisions
  use module_bigdft_arrays
  use module_bigdft_profiling
  use module_bigdft_errors
  use wrapper_linalg
  use locreg_operations, only: workarrays_projectors,NCPLX_MAX
  use at_domain, only: domain_periodic_dims
  use gaussians
  use f_arrays
  use locregs
  use compression
  use f_utils
  implicit none
  integer, intent(in) :: ncplx_k
  real(gp), intent(in) :: rx,ry,rz,kx,ky,kz
  real(gp), intent(in) :: gau_cut
  type(locreg_descriptors), intent(in) :: lr
  type(gaussian_real_space), intent(in) :: g

  type(workarrays_projectors),intent(inout) :: wpr
  real(wp), dimension(array_dim(lr)*ncplx_k), intent(out) :: proj
  !Local variables
  logical :: perx,pery,perz !variables controlling the periodicity in x,y,z
  integer :: iterm,n_gau,ml1,ml2,ml3,mu1,mu2,mu3
  integer :: ncplx_w
  integer :: iskip
  logical, dimension(3) :: peri
  type(f_scalar) :: one
  !Variables for OpenMP
  !!$ integer :: ithread,nthread,ichunk
  !!$ integer :: omp_get_thread_num,omp_get_num_threads

!!  integer :: ncount0,ncount_rate,ncount_max,ncount1,ncount2

  call f_routine(id='crtproj')

  !wproj is complex for PAW and kpoints.
  ncplx_w=max(ncplx_k,1)
  if (imag(g%sigma) /= 0._gp) ncplx_w=2

  ! The workarrays wpr%wprojx, wpr%wprojy, wpr%wprojz are allocated with the
  ! first dimension equal to NCPLX_MAX (which is 2). However the routine gauss_to_daub_k always
  ! assumes the correct value for ncplx_w and thus fills the arrays
  ! contiguously. Therefore in the non-complex case one has to fill the holes in
  ! thw workarrays.
  if (ncplx_w==NCPLX_MAX) then
      iskip = 1
  else
      iskip = 2
  end if

  ! Check the dimensions
  if (size(wpr%wprojx,2)<lr%mesh_coarse%ndims(1)-1) &
       call f_err_throw('workarray wpr%wprojx too small',err_name='BIGDFT_RUNTIME_ERROR')
  if (size(wpr%wprojy,2)<lr%mesh_coarse%ndims(2)-1) &
       call f_err_throw('workarray wpr%wprojy too small',err_name='BIGDFT_RUNTIME_ERROR')
  if (size(wpr%wprojz,2)<lr%mesh_coarse%ndims(3)-1) &
       call f_err_throw('workarray wpr%wprojz too small',err_name='BIGDFT_RUNTIME_ERROR')

  !conditions for periodicity in the three directions
  peri=domain_periodic_dims(lr%mesh%dom)
  perx=peri(1)
  pery=peri(2)
  perz=peri(3)

  one = f_scalar(1._gp, 0._gp)

!!  call system_clock(ncount0,ncount_rate,ncount_max)

  ! OpenMP commented here as doesn't work on Vesta
  !!$omp parallel default(shared) private(iterm,work,ml1,mu1,ml2,mu2,ml3,mu3) &
  !!$omp private(ithread,ichunk,factor,n_gau)

  !!$ ithread=omp_get_thread_num()
  !!$ nthread=omp_get_num_threads()
  !!$ ichunk=0
  do iterm=1,g%nterms
     !!$ ichunk=ichunk+1
     !!$ if (mod(ichunk,nthread).eq.ithread) then
     n_gau=g%lxyz(1, iterm)
     call gauss_to_daub_k(lr%mesh_coarse%hgrids(1),kx*lr%mesh_coarse%hgrids(1),&
          ncplx_w,ncplx_k,g%factors(iterm),rx,g%sigma,n_gau,lr%nboxc(1,1),lr%mesh_coarse%ndims(1)-1,ml1,mu1,&
          wpr%wproj(1),wpr%work,size(wpr%work, 1),perx,gau_cut)
     !!$ endif
     call vcopy(ncplx_w*lr%mesh_coarse%ndims(1), wpr%wproj(1), 1, wpr%wprojx(1,0,1,iterm), iskip)
     call vcopy(ncplx_w*lr%mesh_coarse%ndims(1), wpr%wproj(ncplx_w*lr%mesh_coarse%ndims(1)+1), 1, wpr%wprojx(1,0,2,iterm), iskip)

     !!$ ichunk=ichunk+1
     !!$ if (mod(ichunk,nthread).eq.ithread) then
     n_gau=g%lxyz(2, iterm)
     call gauss_to_daub_k(lr%mesh_coarse%hgrids(2),ky*lr%mesh_coarse%hgrids(2),&
          ncplx_w,ncplx_k,one,ry,g%sigma,n_gau,lr%nboxc(1,2),lr%mesh_coarse%ndims(2)-1,ml2,mu2,&
          wpr%wproj(1),wpr%work,size(wpr%work, 1),pery,gau_cut)
     !!$ endif
     call vcopy(ncplx_w*lr%mesh_coarse%ndims(2), wpr%wproj(1), 1, wpr%wprojy(1,0,1,iterm), iskip)
     call vcopy(ncplx_w*lr%mesh_coarse%ndims(2), wpr%wproj(ncplx_w*lr%mesh_coarse%ndims(2)+1), 1, wpr%wprojy(1,0,2,iterm), iskip)

     !!$ ichunk=ichunk+1
     !!$ if (mod(ichunk,nthread).eq.ithread) then
     n_gau=g%lxyz(3, iterm)
     call gauss_to_daub_k(lr%mesh_coarse%hgrids(3),kz*lr%mesh_coarse%hgrids(3),&
          ncplx_w,ncplx_k,one,rz,g%sigma,n_gau,lr%nboxc(1,3),lr%mesh_coarse%ndims(3)-1,ml3,mu3,&
          wpr%wproj(1),wpr%work,size(wpr%work, 1),perz,gau_cut)
     !!$ endif
     call vcopy(ncplx_w*lr%mesh_coarse%ndims(3), wpr%wproj(1), 1, wpr%wprojz(1,0,1,iterm), iskip)
     call vcopy(ncplx_w*lr%mesh_coarse%ndims(3), wpr%wproj(ncplx_w*lr%mesh_coarse%ndims(3)+1), 1, wpr%wprojz(1,0,2,iterm), iskip)
  end do

  !!$omp end critical
  !!$omp end parallel

  !write(10000+bigdft_mpi%iproc*10,*) wpr%wprojx

!!  call system_clock(ncount1,ncount_rate,ncount_max)
!!  write(20,*) 'TIMING1:', dble(ncount1-ncount0)/dble(ncount_rate)

  call f_zero(proj)
  call lr_accumulate(lr, ncplx_w, g%nterms, wpr%wprojx, wpr%wprojy, wpr%wprojz, ncplx_k, proj)
!!  call system_clock(ncount2,ncount_rate,ncount_max)
!!  write(20,*) 'TIMING2:', dble(ncount2-ncount1)/dble(ncount_rate)

  !call finalize_real_space_conversion()

  call f_release_routine()
END SUBROUTINE crtproj

subroutine plr_segs_and_vctrs(plr,nseg_c,nseg_f,nvctr_c,nvctr_f)
  use locregs
  implicit none
  type(locreg_descriptors), intent(in) :: plr
  integer, intent(out) :: nseg_c,nseg_f,nvctr_c,nvctr_f
  !local variables

  nseg_c=plr%wfd%nseg_c
  nseg_f=plr%wfd%nseg_f
  nvctr_c=plr%wfd%nvctr_c
  nvctr_f=plr%wfd%nvctr_f

end subroutine plr_segs_and_vctrs
