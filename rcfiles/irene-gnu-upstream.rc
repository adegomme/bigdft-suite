#This is the configuration file for the BigDFT installer
##This is a python script which is executed by the build suite
#
##Add the condition testing to run tests and includes PyYaml
conditions.add("bio")
conditions.add("ase")
conditions.add("vdw")
conditions.add("python")
conditions.add("sirius")
#conditions.add("devdoc")
#conditions.add("simulation")
conditions.add("dill")
conditions.add("boost")
conditions.add("amber")
conditions.add("spg")
##List the module the this rcfile will build
modules = ['spred',]

def lapack():
   return ' --with-ext-linalg="-Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lgomp -lpthread -lm -ldl" '

def conf_line():
  return " FC=mpif90 FCFLAGS='-O2 -g -fPIC -fopenmp -m64 -I${MKLROOT}/include' --enable-dynamic-libraries " + lapack()

def env_configuration():
    return conf_line()
##example of the potentialities of the python syntax in this file
#def env_configuration():
#    return  """ FCFLAGS="-I${MKLROOT}/include -O2 -fPIC -qopenmp -g" FC="mpif90 -fc=ifort" CC=icc CFLAGS=-fPIC --with-ext-linalg="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -liomp5 -lpthread -lm -ldl -lstdc++" --with-gobject=yes  --enable-dynamic-libraries CXX=icpc"""
#the following command sets the environment variable to give these settings
#to all the modules
import os
os.environ['BIGDFT_CONFIGURE_FLAGS']=env_configuration()
#here follow the configuration instructions for the modules built
#we specify the configurations for the modules to customize the options if needed
autogenargs = env_configuration()
os.environ['PYGOBJECT_WITHOUT_PYCAIRO']='1'
os.environ['OPENMM_INCLUDE_PATH']=os.path.join(prefix,'include')
os.environ['OPENMM_LIB_PATH']=os.path.join(prefix,'lib')

def get_include_dir():
    from subprocess import check_output
    includes = check_output(['python3-config','--includes'])
    return includes.split()[0][2:].decode("utf-8")

module_autogenargs.update({
            'biopython': "", 'simtk': "", 'pdbfixer': "", 'ase': "", 'dill': "", 'libffi': "CC=gcc",
            'gobject-introspection': "PYTHON=python3", 'dnaviewer': "", 'lsim-f2py': "", 'pygobject': "", 'v_sim-dev': "PYTHON=python3 CC=icc"
            })

module_cmakeargs.update({
#               'rdkit': "-DPYTHON_EXECUTABLE=/opt/intel/oneapi/intelpython/latest/bin/python -DPYTHON_LIBRARY=/opt/intel/oneapi/intelpython/latest/lib -DPYTHON_INCLUDE_DIR="+get_include_dir()
                        })
#
module_cmakeargs['spfft'] = "-DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_BUILD_TYPE=RELEASE -DSPFFT_SINGLE_PRECISION=OFF -DSPFFT_MPI=ON -DSPFFT_OMP=ON " # -DSPFFT_STATIC=ON " # -DSPFFT_GPU_BACKEND=CUDA  -DCMAKE_CUDA_COMPILER=/usr/local/cuda-10.1/bin/nvcc -DCMAKE_CUDA_FLAGS=-ccbin=gcc-8
module_autogenargs['hdf5'] = env_configuration() + " --enable-fortran --disable-deprecated-symbols --disable-filters --disable-parallel --with-zlib=no --with-szlib=no" #--disable-shared --enable-static=yes
module_cmakeargs['sirius'] = "-DUSE_MKL=ON -DMKL_DEF_LIBRARY=${MKLROOT}/lib/intel64"
module_cmakeargs['costa']= "-DCMAKE_CXX_FLAGS=-fPIC"

# Default moduleq for Irene are in use.

#build_policy = "all"
# Specific configure arguments for each module
module_autogenargs['libxc'] = "FC=mpif90 CC=gcc --enable-shared"
module_autogenargs['futile'] = conf_line() + " CFLAGS='-O2 -g -fPIC' "
module_autogenargs['atlab'] = conf_line() 
module_autogenargs['psolver'] =  conf_line() 
module_autogenargs['bigdft'] =  conf_line() 
module_autogenargs['spred'] =  conf_line() 
module_autogenargs['libABINIT'] =   conf_line() 
module_autogenargs['GaIn'] = "FC=mpif90 FCFLAGS='-fPIC'"
module_autogenargs['chess'] =   conf_line() 

module_cmakeargs.update({
    'ntpoly': '-DFORTRAN_ONLY=Yes -DCMAKE_Fortran_COMPILER="mpif90" -DCMAKE_Fortran_FLAGS="-O2 -g -fPIC -fopenmp  -m64 -I${MKLROOT}/include $MPI_FFLAGS"  -DCMAKE_Fortran_FLAGS_RELEASE="-O2 -g -fPIC -fopenmp  -m64 -I${MKLROOT}/include $MPI_FFLAGS" -DBUILD_SHARED_LIBS=Yes -DOpenMP_Fortran_FLAGS=-fopenmp'})
